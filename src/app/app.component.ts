import {Component} from '@angular/core';
import { InitializerService } from './globals/service/initializer/initializer.service';

@Component({
    selector: 'body',
    template: '<router-outlet></router-outlet>'
})
export class AppComponent { 
    constructor(private iS: InitializerService) {
        
    }
}
