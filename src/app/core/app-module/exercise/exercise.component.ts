import { ActivatedRoute, Params, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { ExerciseLocalService } from './_local/exercise.local';
import { ExerciseService } from '../../../api/app-module/exercise/service/generic/exercise/exercise.service';

@Component({
  selector: 'app-exercise',
  templateUrl: 'exercise.component.html',
  styleUrls: ['exercise.component.scss'],
})
export class ExerciseComponent implements OnInit {
	constructor(private router : Router, private eS: ExerciseService, private _local : ExerciseLocalService) {
    
  }

  ngOnInit() {
    this._local.Exercises$ = this.eS.retrieveAllExercises();
  }
}



