import { CategoryDescriptionPipe, CategoryNamePipe } from '../../../globals/pipe/category/category-finder.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import {
    BasicsCreateExerciseComponent,
} from './submodule/create-exercise/subcomponents/basics.create-exercise/basics.component';
import { CategoryService } from '../../../api/app-module/exercise/service/generic/exercise/category/category.service';
import {
    ChartExerciseComponent,
} from './subcomponent/master-detail/subcomponent/chart-exercise/chart-exercise.component';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { CommonModule } from '@angular/common';
import { CreateExerciseComponent } from './submodule/create-exercise/create-exercise.component';
import { CreateExerciseLocalService } from './submodule/create-exercise/service/create-exercise.local.service';
import {
    DependentYLoaderService,
} from '../../../api/app-module/exercise/service/statistical/local-statistics/loader/axis-loader/dependent-y-loader/dependent-y-loader.service';
import {
    DetailExerciseComponent,
} from './subcomponent/master-detail/subcomponent/detail-exercise/detail-exercise.component';
import {
    EquipmentService,
} from '../../../api/app-module/exercise/service/generic/exercise/equipment/equipment.service';
import { ExerciseComponent } from './exercise.component';
import {
    ExerciseDataPointService,
} from '../../../api/app-module/exercise/service/generic/data-point/data-point.service';
import {
    ExerciseGlobalStatisticsService,
} from '../../../api/app-module/exercise/service/statistical/global-statistics/exercise-global.statistics';
import { ExerciseLocalService } from './_local/exercise.local';
import { ExerciseLocalStatisticsService } from './_local/exercise.local.statistics';
import { ExerciseRepository } from '../../../api/app-module/exercise/repository/exercise/exercise.repository';
import { ExerciseRoutingModule } from './exercise.routing';
import { ExerciseService } from '../../../api/app-module/exercise/service/generic/exercise/exercise.service';
import {
    ExerciseStatisticsService,
} from '../../../api/app-module/exercise/service/statistical/exercise/exercise.statistics';
import {
    HeaderCreateExerciseComponent,
} from './submodule/create-exercise/subcomponents/header.create-exercise/header.component';
import { HeaderExerciseComponent } from './subcomponent/header/header-exercise.component';
import {
    IndependentXLoaderService,
} from '../../../api/app-module/exercise/service/statistical/local-statistics/loader/axis-loader/independent-x-loader/independent-x-loader.service';
import {
    Insight1StatisticsExerciseComponent,
} from './subcomponent/statistics/subcomponent/insight1-statistics-exercise/insight1-statistics-exercise.component';
import {
    Insight2StatisticsExerciseComponent,
} from './subcomponent/statistics/subcomponent/insight2-statistics-exercise/insight2-statistics-exercise.component';
import { ListExerciseComponent } from './subcomponent/list/list-exercise.component';
import {
    LoggedExerciseService,
} from '../../../api/app-module/exercise/service/generic/logged-exercise/logged-exercise.service';
import { MasterDetailExerciseComponent } from './subcomponent/master-detail/master-detail-exercise.component';
import { MeasureService } from '../../../api/app-module/exercise/service/generic/exercise/measure/measure.service';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {
    PropertiesCreateExerciseComponent,
} from './submodule/create-exercise/subcomponents/properties.create-exercise/properties.component';
import { StatisticsExerciseComponent } from './subcomponent/statistics/statistics-exercise.component';
import {
    SummaryCreateExerciseComponent,
} from './submodule/create-exercise/subcomponents/summary.create-exercise/summary.component';

@NgModule({
  imports: [
    CommonModule,      
    FormsModule,
    ChartsModule,
    NgbModule.forRoot(),
    ExerciseRoutingModule,
  ],
  declarations: [
    /**PARENT */
    ExerciseComponent, 
  
    /**SUBMODULES*/
    CreateExerciseComponent, BasicsCreateExerciseComponent, 
    SummaryCreateExerciseComponent, PropertiesCreateExerciseComponent, 
    HeaderCreateExerciseComponent,

    /**SUBCOMPONENTS*/
    HeaderExerciseComponent,  DetailExerciseComponent, ChartExerciseComponent, 
    Insight1StatisticsExerciseComponent, Insight2StatisticsExerciseComponent, 
    StatisticsExerciseComponent, MasterDetailExerciseComponent, ListExerciseComponent],
  providers: [
      /**SERVICES */
      ExerciseService, ExerciseLocalStatisticsService, ExerciseDataPointService, 
      ExerciseGlobalStatisticsService, DependentYLoaderService, 
      IndependentXLoaderService, ExerciseStatisticsService, 
      CategoryService, EquipmentService, MeasureService, 
      ExerciseLocalService, LoggedExerciseService, CreateExerciseLocalService,
      
      /**REPOSITORYS */
      ExerciseRepository,


      /**PIPES */
        CategoryNamePipe, CategoryDescriptionPipe
      ]
})
export class ExerciseModule { }
