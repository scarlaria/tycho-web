import {Injectable, group} from '@angular/core';
import { Observable, ReplaySubject, Subject } from 'rxjs/Rx';

import { FirebaseListObservable } from 'angularfire2/database';

@Injectable()
export class ExerciseLocalService {

	private exercises$ : Observable<{}[]>;
    private searchedExercises$: Observable<{}[]>;    
	private currentIndex$: ReplaySubject<number>;
    private searchFilter$ :Subject<string>;
	
    private masterDetailToggle: number;
    private toggleMaster: number;

	
    constructor() { 
		this.exercises$ = new Observable<{}[]>(x=>x)
		this.currentIndex$ = new ReplaySubject<number>(2);
		this.currentIndex$.next(0);
		this.searchFilter$ = new Subject<string>();
		this.searchFilter$.next('');
        this.searchedExercises$ = new Observable<{}[]>(x=>x);
        this.masterDetailToggle = 0;
        this.toggleMaster = 0;
	}

	public get Exercises$(): Observable<{}[]> {
		return this.exercises$;
	}

	public set Exercises$(value: Observable<{}[]>) {
		this.exercises$ = value;
	}

	public get SearchedExercises$(): Observable<{}[]> {
		return this.searchedExercises$;
	}

	public set SearchedExercises$(value: Observable<{}[]>) {
		this.searchedExercises$ = value;
	}

    public get ToggleMaster(): number {
		return this.toggleMaster;
	}

	public set ToggleMaster(value: number) {
		this.toggleMaster = value;
	}
    

	public get SearchFilter$(): Subject<string> { return this.searchFilter$; }
	public set SearchFilter$(value: Subject<string>) { this.searchFilter$ = value; }
    public get MasterDetailToggle(): number {return this.masterDetailToggle;}
	public set MasterDetailtoggle(value: number) {this.masterDetailToggle = value;}

	public get CurrentIndex$(): ReplaySubject<number> {
		return this.currentIndex$;
	}

	public set CurrentIndex$(value: ReplaySubject<number>) {
		this.currentIndex$ = value;
	}
	
}