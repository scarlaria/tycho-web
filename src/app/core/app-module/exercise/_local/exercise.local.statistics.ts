import { BehaviorSubject, Observable, ReplaySubject } from 'rxjs/Rx';

import {AdaptiveViewModel} from '../../../../api/dynamic-library/entity/adaptive-view-model/adaptive-view-model';
import { Injectable } from '@angular/core';

@Injectable()
export class ExerciseLocalStatisticsService {

    // private exerciseStatistics : ExerciseStatisticsViewEntity[];
	private statistics$: Observable<Observable<AdaptiveViewModel<{}>>>;
	private index: number;
	private index$: BehaviorSubject<number>;

    constructor() {
        // this.exerciseStatistics = new Array<ExerciseStatisticsViewEntity>();
		this.statistics$ = new Observable<Observable<AdaptiveViewModel<{}>>>();
		this.index = 0;
		this.index$ = new BehaviorSubject(2);
    }

	
	// public get ExerciseStatistics(): ExerciseStatisticsViewEntity[] {
	// 	return this.exerciseStatistics;
	// }

	// public set ExerciseStatistics(value: ExerciseStatisticsViewEntity[]) {
	// 	this.exerciseStatistics = value;
	// }

	public get Statistics$(): Observable<Observable<AdaptiveViewModel<{}>>> {
		return this.statistics$;
	}

	public set Statistics$(value: Observable<Observable<AdaptiveViewModel<{}>>>) {
		this.statistics$ = value;
	}

	public get Index$(): BehaviorSubject<number> {
		return this.index$;
	}

	public set Index$(value: BehaviorSubject<number>) {
		this.index$ = value;
	}

	public get Index(): number {
		return this.index;
	}

	public set Index(value: number) {
		this.index = value;
	}
}