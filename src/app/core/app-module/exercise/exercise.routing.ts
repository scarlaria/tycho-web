import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';
import { ExerciseComponent } from './exercise.component';
import { CreateExerciseComponent } from './submodule/create-exercise/create-exercise.component';

const routes: Routes = [
  {
    path: '',
    component: ExerciseComponent,
    data: {
      title: 'Exercises'
    }
  },
    {
    path: 'create-exercise',
    component: CreateExerciseComponent,
    data: {
      title: 'Create-Exercise'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExerciseRoutingModule {}
