import { Component, OnInit } from '@angular/core';

import {AdaptiveViewModel} from '../../../../../../../api/dynamic-library/entity/adaptive-view-model/adaptive-view-model';
import {CreateExerciseLocalService} from '../../service/create-exercise.local.service';
import {
    ExerciseService,
} from '../../../../../../../api/app-module/exercise/service/generic/exercise/exercise.service';

/**
 * @author Scarlaria Studios (2017)
 */

@Component({
	selector: 'app-header-create-exercise',
	templateUrl: 'header.component.html',
})

/**
 * Is a subcomponent of the CreateExerciseComponent.
 */
export class HeaderCreateExerciseComponent implements OnInit {

	private headers : AdaptiveViewModel<String>[];
	private title: string;

	constructor(private eS :ExerciseService, private _local : CreateExerciseLocalService) {
		this.headers = new Array<AdaptiveViewModel<String>>();
		this.title = "Create an Exercise";
		this.initPageHeaders();
		console.log(this.headers);
	}

	ngOnInit() { 
		
	}

	/**
	 * Initialises all the page headers, Basic, Properties, Settings, Summart
	 */
	initPageHeaders() {
		let basicHeader = this.initBasicHeader();
		let propertiesHeader = this.initPropertiesHeader();
		let summaryHeader = this.initSummaryHeader();
		
		this.headers.push(basicHeader);
		this.headers.push(propertiesHeader);
		this.headers.push(summaryHeader);
		this._local.TotalPages = this.headers.length;
	}

	/**
	 * Initialises the basic header by creating and returning an AdaptiveViewModel
	 * @returns AdaptiveViewModel<String>
	 */
	private initBasicHeader() : AdaptiveViewModel<String> {
		let basicHeader  = new AdaptiveViewModel<String>(0,this._nav_link_active, 'Basics');
		basicHeader.ElementName = 'Basics Header'
		return basicHeader;
	}

	/**
	 * Initialises the properties header by creating and returning an AdaptiveViewModel
	 * @returns AdaptiveViewModel<String>
	 */
	private initPropertiesHeader() : AdaptiveViewModel<String> {
		let propertiesHeader  = new AdaptiveViewModel<String>(1, this._nav_link, 'Properties');
		propertiesHeader.ElementName = 'Properties Header';
		return propertiesHeader;
	}

	/**
	 * Initialises the summary header by creating and returning an AdaptiveViewModel
	 * @returns AdaptiveViewModel<String>
	 */
	private initSummaryHeader() : AdaptiveViewModel<String> {
		let summaryHeader  = new AdaptiveViewModel<String>(1, this._nav_link, 'Summary');
		summaryHeader.ElementName = 'Summarys Header';
		return summaryHeader
	}


	/**
	 * Manipulates the current page when the index of a header is selected.
	 * @param i 
	 */
	onHeaderLinkClick(i : number) {	
		if(i != this._local.CurrentPage){
			this.headers[this._local.CurrentPage].ClassInfo = this._nav_link;
			this.headers[i].ClassInfo = this._nav_link_active;
			this._local.CurrentPage = i;
		}
	}
	

  	/**
	   * Navigation Menu
	   */	
	  _nav_link = {
	    'nav-link': true,
	    'active': false
	  }
	  _nav_link_active = {
	     'nav-link': true,
	      'active': true   
	  }


	public get Headers(): AdaptiveViewModel<String>[] {
		return this.headers;
	}

	public set Headers(value: AdaptiveViewModel<String>[]) {
		this.headers = value;
	}

}

export enum PageHeaders {
	Basics = 0,
	Properties = 1,
	Settings = 2,
	Summary = 3
}