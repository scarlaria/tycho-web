import { Component, OnInit } from '@angular/core';

import {CategoryService} from '../../../../../../../api/app-module/exercise/service/generic/exercise/category/category.service';
import {CreateExerciseLocalService} from '../../service/create-exercise.local.service';
import {EquipmentService} from '../../../../../../../api/app-module/exercise/service/generic/exercise/equipment/equipment.service';
import {ExerciseService} from '../../../../../../../api/app-module/exercise/service/generic/exercise/exercise.service';
import {MeasureService} from '../../../../../../../api/app-module/exercise/service/generic/exercise/measure/measure.service';
import {Router} from '@angular/router';

@Component({
	selector: 'app-summary-create-exercise',
	templateUrl: 'summary.component.html',
})

export class SummaryCreateExerciseComponent implements OnInit {

	constructor(private eS: ExerciseService, private _local : CreateExerciseLocalService, 
                private router : Router, private mS : MeasureService, private cS : CategoryService, 
                private eqS : EquipmentService) {
		
	}

	ngOnInit() { }

  /**
   * This method uploads the Exercise to the database upon completing checks.
   * VALIDATION MUST HAVE COMPLETED | BE SATISFIED in order for this method to be invoked
   */
	public create(){ 

    /**Add automated elements of an exercise e.g createdDate etc. */
        // let exercise = this.eS.autoCompleo(this._local.Exercise);

    /**Purge string<array> ids to produce object with ids. */
        let categorys = this.cS.purge(this._local.Exercise.Category);
        // let measures = this.mS.purge(this._local.Exercise.Measure);
        let equipments = this.eqS.purge(this._local.Exercise.Equipment);

    /**Purge the final exercise to produce an object worthy of the database */
        // let purgedExercise = this.eS.purge(exercise, categorys, measures, equipments, {});

    /**Create that object by uploading to the firebase database */
        // let createRef = this.eS.createObject(purgedExercise)  
        // createRef.then(
        //   resolve =>{
        //      this.router.navigate(['/exercises']);
        // });
    }
}