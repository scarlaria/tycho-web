import { Component, OnInit } from '@angular/core';

import {AdaptiveViewModel} from '../../../../../../../api/dynamic-library/entity/adaptive-view-model/adaptive-view-model';
import {CategoryEntity} from '../../../../../../../api/app-module/exercise/entity/exercise/category/category.entity';
import {CategoryService} from '../../../../../../../api/app-module/exercise/service/generic/exercise/category/category.service';
import {CreateExerciseLocalService} from '../../service/create-exercise.local.service';
import {EquipmentEntity} from '../../../../../../../api/app-module/exercise/entity/exercise/equipment/equipment.entity';
import {EquipmentService} from '../../../../../../../api/app-module/exercise/service/generic/exercise/equipment/equipment.service';
import {
    ExerciseService,
} from '../../../../../../../api/app-module/exercise/service/generic/exercise/exercise.service';
import {MeasureEntity} from '../../../../../../../api/app-module/exercise/entity/exercise/measure/measure.entity';
import {MeasureService} from '../../../../../../../api/app-module/exercise/service/generic/exercise/measure/measure.service';

@Component({
	selector: 'app-properties-create-exercise',
	templateUrl: 'properties.component.html'
})

export class PropertiesCreateExerciseComponent implements OnInit {

	private categoryButtons : AdaptiveViewModel<CategoryEntity>[];
	private measureButtons : AdaptiveViewModel<MeasureEntity>[];
	private equipmentButtons : AdaptiveViewModel<EquipmentEntity>[];

	private categoryPills : string[];
	private measurePills: string[];
	private equipmentPills: string[]

	constructor(private eS :ExerciseService, 
				private _local : CreateExerciseLocalService,
				private eqS : EquipmentService, 
				private mS : MeasureService, 
				private cS : CategoryService) {

        this.categoryButtons = new Array<AdaptiveViewModel<CategoryEntity>>();
        this.measureButtons = new Array<AdaptiveViewModel<MeasureEntity>>();
        this.equipmentButtons = new Array<AdaptiveViewModel<EquipmentEntity>>();

		this.categoryPills = new Array<string>();	
		this.measurePills = new Array<string>();	
		this.equipmentPills = new Array<string>();	

	}

	ngOnInit() { 
		this.initCategorys();		
		this.initMeasures();
		this.initEquipments();
	}
	
	/**
	 * Checks to see if no categorys have been selected. 
	 * If some have it finds them based on the id and updates the view
	 */
	getSelectedCategorys() {
		if(this._local.Exercise.Category) {
			this._local.Exercise.Category.forEach((id, index) => {
				if(id == this.categoryButtons[index].Entity.Id) {
					this.categoryButtons[index].ClassInfo = this._category_button_active;
					this.categoryPills.push(this.categoryButtons[index].Entity.Name)
				}
			})
		}
	}

	findAdaptiveViewIndexByString(item : string, aVMS : AdaptiveViewModel<string>[]) : number {
		aVMS.forEach((elem, i) => {
			if(elem.Entity == item) {
				return i;
			}
		})
		return -1;
	}

	/**
	 * Downloads categorys from the database and returns then in an array of ExerciseCategoryEntity;
	 * @returns ExerciseCategoryEntity[]
	 */
	initCategorys(){
		let categoryEntities = new Array<CategoryEntity>();
		this.cS.retrieveAllCategorys().subscribe(categorys => {
			categorys.forEach((category,index) => {
				/**Create a category object by converting the incoming category object to a category entity*/
				let categoryEntity = CategoryEntity.convertObjectToExerciseCategory(category['$key'], category);
				categoryEntities.push(categoryEntity);
				this.categoryButtons.push(new AdaptiveViewModel<CategoryEntity>(index, this._category_button, categoryEntity))
			})
		});
		this._local.Categorys = categoryEntities;
	}
	
	initMeasures(): MeasureEntity[] {
		let measureEntities = new Array<MeasureEntity>();
		// this.mS.retrieveMeasures().subscribe(measures => {
		// 	measures.forEach((measure,index) => {
		// 		/**Create a measure object by converting the incoming measure object to a measure entity*/
		// 		let measureEntity = MeasureEntity.convertObjectToExerciseMeasure(measure['$key'], measure);
		// 		this.measureButtons.push(new AdaptiveViewModel<MeasureEntity>(index, this._category_button, measureEntity))			
		// 		measureEntities.push(measureEntity);
		// 	})
		// })
		// this._local.Measures = measureEntities;
		return measureEntities;
	}

	initEquipments() : EquipmentEntity[] {
		let equipmentEntities = new Array<EquipmentEntity>();
		// this.eqS.retrieveEquipments().subscribe(equipments => {
		// 	equipments.forEach((equipment, index) => {
		// 		/**Create a equipment object by converting the incoming equipment object to a equipment entity*/
		// 		let equipmentEntity = EquipmentEntity.convertObjectToExerciseEquipment(equipment['$key'], equipment);
		// 		this.equipmentButtons.push(new AdaptiveViewModel<EquipmentEntity>(index, this._category_button, equipmentEntity))							
		// 		equipmentEntities.push(equipmentEntity);
		// 	})
		// })
		// this._local.Equipments = equipmentEntities;
		return equipmentEntities;
	}

	findDuplicateIndex(elem : string, arr : string[]) : number {
		arr.forEach((item, index)=> {
			if(elem == item) {
				return index;
			}
		})
		return -1;
	}

	onCategoryButtonClick(i : number) {

		/**1. If button is not clicked 
		 * change button to active and add to list;
		 * 2. If button is clicked
		 * change button to not active and remove from list;
		 */
		let category = this.CategoryButtons[i].Entity;
		if(this.CategoryButtons[i].ClassInfo['active'] == false) {
			this.CategoryButtons[i].ClassInfo = this._category_button_active;
			console.log(category);		
			this._local.Exercise.Category.push(category.Id);
			//AddPill
			this.categoryPills.push(category.Name);
		} else {
			this.CategoryButtons[i].ClassInfo = this._category_button;
			let val = this.findDuplicateIndex(category.Id, this._local.Exercise.Category);
			if(val != -1) {
				this._local.Exercise.Category.push(category.Id);
				this.categoryPills.push(category.Name);
			}else{
				this._local.Exercise.Category.splice(val);
				this.categoryPills.splice(val);
			}
		}		
	};

	onMeasureButtonClick(i : number) {
		let measure = this.MeasureButtons[i].Entity;
		if(this.MeasureButtons[i].ClassInfo['active'] == false) {
			this.MeasureButtons[i].ClassInfo = this._measure_button_active;
			console.log(measure);		
			this._local.Exercise.Measure.push(measure.Id);
			//AddPill
			this.measurePills.push(measure.Name);
		} else {
			this.MeasureButtons[i].ClassInfo = this._measure_button;
			let val = this.findDuplicateIndex(measure.Id, this._local.Exercise.Measure);
			if(val != -1) {
				this._local.Exercise.Measure.push(measure.Id);
				this.measurePills.push(measure.Name);
			}else{
				this._local.Exercise.Measure.splice(val);
				this.measurePills.splice(val);
			}
		}
	}

	onEquipmentButtonClick(i : number) {
		let equipment = this.EquipmentButtons[i].Entity;
		if(this.EquipmentButtons[i].ClassInfo['active'] == false) {
			this.EquipmentButtons[i].ClassInfo = this._equipment_button_active;
			console.log(equipment);		
			this._local.Exercise.Equipment.push(equipment.Id);
			//AddPill
			this.equipmentPills.push(equipment.Name);
		} else {
			this.EquipmentButtons[i].ClassInfo = this._equipment_button;
			let val = this.findDuplicateIndex(equipment.Id, this._local.Exercise.Equipment);
			if(val != -1) {
				this._local.Exercise.Equipment.push(equipment.Id);
				this.equipmentPills.push(equipment.Name);
			}else{
				this._local.Exercise.Equipment.splice(val);
				this.equipmentPills.splice(val);
			}
		}
	}

	/**Initialises the group buttons for the cateogry */
	 public  _category_button = {
	    'btn' : true,
	    'btn-secondary' : true,
	    'active' : false
	  }
	 public _category_button_active = {
	    'btn' : true,
	    'btn-success' : true,
	    'active' : true
	  }

	  	 public  _measure_button = {
	    'btn' : true,
	    'btn-secondary' : true,
	    'active' : false
	  }
	 public _measure_button_active = {
	    'btn' : true,
	    'btn-success' : true,
	    'active' : true
	  }

	  	 public  _equipment_button = {
	    'btn' : true,
	    'btn-secondary' : true,
	    'active' : false
	  }
	 public _equipment_button_active = {
	    'btn' : true,
	    'btn-success' : true,
	    'active' : true
	  }

	/**GETTER SETTER */
	public get CategoryButtons(): AdaptiveViewModel<CategoryEntity>[] {
		return this.categoryButtons;
	}

	public set CategoryButtons(value: AdaptiveViewModel<CategoryEntity>[]) {
		this.categoryButtons = value;
	}

	public get MeasureButtons(): AdaptiveViewModel<MeasureEntity>[] {
		return this.measureButtons;
	}

	public set MeasureButtons(value: AdaptiveViewModel<MeasureEntity>[]) {
		this.measureButtons = value;
	}

	public get EquipmentButtons(): AdaptiveViewModel<EquipmentEntity>[] {
		return this.equipmentButtons;
	}

	public set EquipmentButtons(value: AdaptiveViewModel<EquipmentEntity>[]) {
		this.equipmentButtons = value;
	}
	
	// public get CurrentEquipment(): number {
	// 	return this.currentEquipment;
	// }

	// public set CurrentEquipment(value: number) {
	// 	this.currentEquipment = value;
	// }

	// public get CurrentCategory(): number {
	// 	return this.currentCategory;
	// }

	// public set CurrentCategory(value: number) {
	// 	this.currentCategory = value;
	// }

	// public get CurrentMeasure(): number {
	// 	return this.currentMeasure;
	// }

	// public set CurrentMeasure(value: number) {
	// 	this.currentMeasure = value;
	// }



}