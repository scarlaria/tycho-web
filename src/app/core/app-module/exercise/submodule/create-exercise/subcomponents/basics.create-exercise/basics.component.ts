import { Component, OnInit } from '@angular/core';

import {CreateExerciseLocalService} from '../../service/create-exercise.local.service';
import {
    ExerciseService,
} from '../../../../../../../api/app-module/exercise/service/generic/exercise/exercise.service';

@Component({
	selector: 'app-basics-create-exercise',
	templateUrl: 'basics.component.html',
})

export class BasicsCreateExerciseComponent implements OnInit {

	constructor(private eS :ExerciseService, private _local : CreateExerciseLocalService) {
		
	}

	ngOnInit() { }

	toggleIsPrivate(e) {
		if(e.target.checked) {
			this._local.Exercise.IsPrivate = true;
		}else{
			this._local.Exercise.IsPrivate = false;
		}
	}
}