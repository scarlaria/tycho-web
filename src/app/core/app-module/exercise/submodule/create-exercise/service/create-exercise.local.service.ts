import {CategoryEntity} from '../../../../../../api/app-module/exercise/entity/exercise/category/category.entity';
import {EquipmentEntity} from '../../../../../../api/app-module/exercise/entity/exercise/equipment/equipment.entity';
import {ExerciseEntity} from '../../../../../../api/app-module/exercise/entity/exercise/exercise.entity';
import { FirebaseListObservable } from 'angularfire2/database';
import { Injectable } from '@angular/core';
import {MeasureEntity} from '../../../../../../api/app-module/exercise/entity/exercise/measure/measure.entity';

@Injectable()
export class CreateExerciseLocalService {
	/**Represents the entity being manipulated in order to create an exercise */
    private exercise : ExerciseEntity;
	private currentPage : number;
	private totalPages : number;

	private categorys : Array<CategoryEntity>;
	private measures :  Array<MeasureEntity>;
	private equipments : Array<EquipmentEntity>;

    constructor() { 
		this.exercise = ExerciseEntity.hyperInit(new ExerciseEntity());
		this.currentPage = 0;
		this.totalPages = 0;
	}

	public get Exercise(): ExerciseEntity {
		return this.exercise;
	}

	public set Exercise(value: ExerciseEntity) {
		this.exercise = value;
	}

	public get CurrentPage(): number {
		return this.currentPage;
	}

	public set CurrentPage(value: number) {
		this.currentPage = value;
	}

	public get TotalPages(): number {
		return this.totalPages;
	}

	public set TotalPages(value: number) {
		this.totalPages = value;
	}

	public get Categorys(): Array<CategoryEntity> {
		return this.categorys;
	}

	public set Categorys(value: Array<CategoryEntity>) {
		this.categorys = value;
	}

	public get Equipments(): Array<EquipmentEntity> {
		return this.equipments;
	}

	public set Equipments(value: Array<EquipmentEntity>) {
		this.equipments = value;
	}

	public get Measures():  Array<MeasureEntity> {
		return this.measures;
	}

	public set Measures(value:  Array<MeasureEntity>) {
		this.measures = value;
	}
	

}