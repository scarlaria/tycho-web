import { Component, HostListener, OnInit } from '@angular/core';

import {CreateExerciseLocalService} from './service/create-exercise.local.service';
import {ROUTE_EXERCISE} from '../../../../../globals/const/global.const';
import {Router} from '@angular/router';

@Component({
  templateUrl: 'create-exercise.component.html',
})

export class CreateExerciseComponent implements OnInit {
  
  constructor(private _local : CreateExerciseLocalService,
             private router : Router) {
      // this.initPageHeaders();
   }

  ngOnInit() {   
  }

  public RDExercise(){
    this.router.navigate([ROUTE_EXERCISE]);
  }

  onAlertClose(index : number) {

  }

  goToPage(i) {
    if(i > this._local.TotalPages && i > -1) {
      this._local.CurrentPage = i;
    }
  }

  /**
   * Cycles to the next page
   */
  cycleNext() {
    if ((this._local.CurrentPage+1) <= this._local.TotalPages){
      this._local.CurrentPage++;
    }
    // this.onNavLinkClickCycle('next');
  }

  /**
   * Cycles to the previous page
   */
  cyclePrev() {
    if ((this._local.CurrentPage-1) >= -1) {
      this._local.CurrentPage--;
    }
    // this.onNavLinkClickCycle('prev');
  }

    @HostListener('window:keydown', ['$event'])
    onKeyboardClick(event: KeyboardEvent) {
      if (event.keyCode == 37) {
            this.cyclePrev();
      }else if (event.keyCode == 39){
            this.cycleNext();
      }
    }

    get diagnostic() { return JSON.stringify(this._local.Exercise); }
}