import {Component, OnInit} from '@angular/core';
import { Observable, Subject } from 'rxjs/Rx';
import {ROUTE_CREATE_EXERCISE, ROUTE_EXERCISE} from '../../../../../globals/const/global.const';

import {ExerciseLocalService} from '../../_local/exercise.local';
import {ExerciseService} from '../../../../../api/app-module/exercise/service/generic/exercise/exercise.service';
import {GlobalService} from '../../../../../api/dynamic-library/service/global/global.service';
import {Router} from '@angular/router';

@Component({
	selector: 'sub-header-exercise',
	templateUrl: 'header-exercise.component.html'
})

export class HeaderExerciseComponent implements OnInit {

		private searchTerm$ = new Subject<string>();
		constructor(private router : Router, 
				private eS: ExerciseService, 
				private _local : ExerciseLocalService) {
			this.searchExercise();
		}

	ngOnInit() { }

	RD_CreateGoal(){
		// this.router.navigate([ROUTE_EXERCISE + '/'+ ROUTE_CREATE_EXERCISE])
	}

	RD_CreateExercise() {
		
	}
	
	/**Step1. Enable  search to locate a list of suitable responses  */
	searchExercise(){
		//this._local.SearchedExercises$ = Observable.empty();
		this._local.SearchedExercises$ = this.search(this._local.SearchFilter$)
	}

	search(terms: Subject<string>, searchBy?: Function): Observable<{}[]> {
		let a = terms
		.debounceTime(400)
		.distinctUntilChanged()
		.switchMap(term => (this.eS.search(this._local.Exercises$, 'name', term)))	
		return GlobalService.toArray(a);											
	}
}