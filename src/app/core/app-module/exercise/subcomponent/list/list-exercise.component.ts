import {Component, OnInit} from '@angular/core';
import { Observable, ReplaySubject, Subject } from 'rxjs/Rx';
import {ROUTE_CREATE_EXERCISE, ROUTE_EXERCISE, ROUTE_VIEW_EXERCISE} from '../../../../../globals/const/global.const';

import {AdaptiveViewModel} from '../../../../../api/dynamic-library/entity/adaptive-view-model/adaptive-view-model';
import {ExerciseLocalService} from '../../_local/exercise.local';
import {ExerciseService} from '../../../../../api/app-module/exercise/service/generic/exercise/exercise.service';
import {
    LoggedExerciseEntity,
} from '../../../../../api/app-module/exercise/entity/logged-exercise/logged-exercise.entity';
import {Router} from '@angular/router';

@Component({
	selector: 'sub-list-exercise',
	templateUrl: 'list-exercise.component.html',
})

export class ListExerciseComponent implements OnInit {

	private adaptiveExerciseList$: Observable<AdaptiveViewModel<{}>[]>
	private adaptiveExercises: AdaptiveViewModel<{}>[];


	constructor(private router : Router, private eS: ExerciseService, private _local : ExerciseLocalService) {
		this.adaptiveExercises = new Array<AdaptiveViewModel<{}>>();
		this.adaptiveExerciseList$ = new Observable<AdaptiveViewModel<{}>[]>();	
	}

	ngOnInit() {
		/**Initialise the adaptive exercise list*/
		this.adaptiveExerciseList$ = this.materialiseExercises();
	}

	materialiseExercises(): Observable<AdaptiveViewModel<{}>[]> {
		return this._local.Exercises$.map(exercises => 
			this.adaptiveExercises.concat(exercises.map((exerciseObject,index) =>{
				return this.convertExerciseToAdaptiveViewModel(exerciseObject, index);
			}))
		)
	}

	convertExerciseToAdaptiveViewModel(exerciseObject: {}, index: number, active: number = 0): AdaptiveViewModel<{}> {
		let avm: AdaptiveViewModel<{}>;
		if(index == active){
			avm = new AdaptiveViewModel(index, this._list_active, exerciseObject);
		}else{
			avm = new AdaptiveViewModel(index, this._list, exerciseObject);
		}	
		return avm;
	}

	onListItemClick(index: number){	 
		this._local.CurrentIndex$.next(index);
		this.changeIndexClassInfo();
  	}

	changeIndexClassInfo() {
		this._local.CurrentIndex$.elementAt(0)
			.map(index => this.adaptiveExercises.map(avm => avm[index].ClassInfo = this._list))

		this._local.CurrentIndex$.elementAt(1)
			.map(index => this.adaptiveExercises.map(avm => avm[index].ClassInfo = this._list_active))
	}

	/**
	 * Retrieves the index on the list
	 * @param index 
	 */
	onMouseOverDelete(index : number) {

	}

	/**
	 * Retrieves the index on the list
	 * @param index 
	 */
	onMouseOverUpdate(index : number) {

	}


	/**
	 * Redirects to create
	 * @param index 
	 */
	RD_Create() {
		this.router.navigate([ROUTE_EXERCISE + '/' + ROUTE_CREATE_EXERCISE]);
	}

	/**
	 * Retrieves the objects id and calls the delete method from the service
	 * @param index 
	 */
	onClickDelete(index : string) {
		this.eS.delete(index).then(x=> {
			console.log('Exercise ' + index + ' has been removed');
		})
	}

	/**
	 * Retrieves the objects id
	 * @param index 
	 */
	onClickView(index : string) {
		this.router.navigate([ROUTE_EXERCISE + '/' + ROUTE_VIEW_EXERCISE], index);
	}

	/**
	 * Retrieves the objects id
	 * @param index 
	 */
	onClickUpdate(index : string) {
		this.router.navigate([ROUTE_EXERCISE + '/' + ROUTE_VIEW_EXERCISE], index);
	}

	/**NGCLASS ATTRIBUTES */
    public _list = {
      'list-group-item' : true,
      'list-group-item-action' : true,
      'flex-column' : true,
      'align-items-start' : true,
      'active' : false
    }

    public _list_active = {
      'list-group-item' : true,
      'list-group-item-action' : true,
      'flex-column' : true,
      'align-items-start' : true,
      'active' : true
    }
}