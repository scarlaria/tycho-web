import { Component, OnInit } from '@angular/core';

import {ExerciseLocalService} from '../../_local/exercise.local';

@Component({
  selector: 'sub-master-detail-exercise',
  templateUrl: './master-detail-exercise.component.html',
  styleUrls: ['./master-detail-exercise.component.scss']
})
export class MasterDetailExerciseComponent implements OnInit {

  private counter: number;
  private toggler: boolean;
  constructor(private _local: ExerciseLocalService) {
    this._local.ToggleMaster = 0;
  }

  ngOnInit() {
  }

  toggleMasterDetail() {
    this._local.ToggleMaster++;
    if(this._local.ToggleMaster % 2 == 0) {
      this.toggler = false;
    }else{
      this.toggler = true;
    }
  }

}