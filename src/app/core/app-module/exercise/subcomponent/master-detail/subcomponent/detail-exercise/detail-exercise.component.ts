import {Component, OnInit} from '@angular/core';

import {
    CategoryService,
} from '../../../../../../../api/app-module/exercise/service/generic/exercise/category/category.service';
import {
    EquipmentService,
} from '../../../../../../../api/app-module/exercise/service/generic/exercise/equipment/equipment.service';
import {ExerciseLocalService} from '../../../../_local/exercise.local';
import {ExerciseService} from '../../../../../../../api/app-module/exercise/service/generic/exercise/exercise.service';
import { InitializerService } from '../../../../../../../globals/service/initializer/initializer.service';
import {
    MeasureService,
} from '../../../../../../../api/app-module/exercise/service/generic/exercise/measure/measure.service';
import { Observable } from 'rxjs/Rx';
import {Router} from '@angular/router';

@Component({
	selector: 'sub-sub-detail-master-detail-exercise',
	templateUrl: 'detail-exercise.component.html',
})

export class DetailExerciseComponent implements OnInit {

	private currentExercise$: Observable<{}>;
	private categorys$: Observable<{}[]>;
	private measures$: Observable<{}[]>;
	private equipments$: Observable<{}[]>;

	constructor(private router : Router, private eqS: EquipmentService, private mS: MeasureService, private cS: CategoryService, private eS: ExerciseService, private _local : ExerciseLocalService, private iS: InitializerService) {	
		this.currentExercise$ = new Observable<{}>(x=>x);
		this.categorys$ = new Observable<{}>(x=>x);
		this.measures$ = new Observable<{}>(x=>x);
		this.equipments$ = new Observable<{}>(x=>x);
	}

	ngOnInit() {
		this.currentExercise$ = this.getSelectedExercises()
		this.categorys$ = this.getCategorys();
		this.measures$  = this.getMeasures();
		this.equipments$ = this.getEquipments();
	}

	getSelectedExercises(): Observable<{}> {
		return this._local.Exercises$
						.combineLatest(this._local.CurrentIndex$, 
							(x,y) => x[y])	 
	}

	getCategorys(): Observable<{}[]>{	
		return this.currentExercise$.map(exerciseObject => {console.log(exerciseObject); return Object.keys(<Array<string>>exerciseObject['category'])})
			.map(categorys => this.cS.retrieveCategorys(categorys))
			.flatMap(x=>x)
	}

	getMeasures(): Observable<{}[]>{	
		return this.currentExercise$.map(exerciseObject => {console.log(exerciseObject); return Object.keys(<Array<string>>exerciseObject['measure'])})
			.map(measures => this.mS.retrieveMeasures(measures))
			.flatMap(x=>x)
	}

	getEquipments(): Observable<{}[]>{	
		return this.currentExercise$.map(exerciseObject => {console.log(exerciseObject); return Object.keys(<Array<string>>exerciseObject['equipment'])})
			.map(equipments => this.eqS.retrieveEquipments(equipments))
			.flatMap(x=>x)
	}


}