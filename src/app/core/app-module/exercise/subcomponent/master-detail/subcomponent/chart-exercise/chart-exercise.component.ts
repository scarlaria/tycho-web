import { Component, OnInit } from '@angular/core';

import {ExerciseLocalService} from '../../../../_local/exercise.local';
import {ExerciseLocalStatisticsService} from '../../../../_local/exercise.local.statistics';
import {ExerciseService} from '../../../../../../../api/app-module/exercise/service/generic/exercise/exercise.service';
import {ExerciseStatisticsService} from '../../../../../../../api/app-module/exercise/service/statistical/exercise/exercise.statistics';
import { Observable } from 'rxjs/Rx';

@Component({
  selector: 'sub-sub-chart-master-detail-exercise',
  templateUrl: './chart-exercise.component.html',
  styleUrls: ['./chart-exercise.component.scss']
})
export class ChartExerciseComponent implements OnInit {

  // private lineChartEntity: LineChartEntity;
  private currentExercise = '-KbyjAO9kYzvPhEbMKlX';

  constructor(private eS: ExerciseService,
              private _local: ExerciseLocalService, 
              private esS: ExerciseStatisticsService, 
              private _localStats: ExerciseLocalStatisticsService) { 

  }

  ngOnInit() {

  }

    // events
  public chartClicked(e:any):void {
    console.log(e);
  }
 
  public chartHovered(e:any):void {
    console.log(e);
  }

}