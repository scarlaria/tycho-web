import { Component, OnInit } from '@angular/core';

import { AdaptiveViewModel } from '../../../../../api/dynamic-library/entity/adaptive-view-model/adaptive-view-model';
import {
    ExerciseGlobalStatisticsService,
} from '../../../../../api/app-module/exercise/service/statistical/global-statistics/exercise-global.statistics';
import { ExerciseLocalService } from '../../_local/exercise.local';
import { ExerciseLocalStatisticsService } from '../../_local/exercise.local.statistics';
import { ExerciseService } from '../../../../../api/app-module/exercise/service/generic/exercise/exercise.service';
import {
    ExerciseStatisticsService,
} from '../../../../../api/app-module/exercise/service/statistical/exercise/exercise.statistics';
import {
    LoggedExerciseService,
} from '../../../../../api/app-module/exercise/service/generic/logged-exercise/logged-exercise.service';
import { Observable } from 'rxjs/Rx';

@Component({ 
	selector: 'sub-statistics-exercise',
	templateUrl: 'statistics-exercise.component.html',

})

export class StatisticsExerciseComponent implements OnInit {


	constructor(private _local_statistics : ExerciseLocalStatisticsService,
				private eSS : ExerciseStatisticsService, 
				private es : ExerciseService, 
				private _local : ExerciseLocalService,
				private egS: ExerciseGlobalStatisticsService,
				private leS: LoggedExerciseService) {

	this.initialiseGlobalInsight();
	this.initialiseLocalInsight();

	}
	ngOnInit() { 

	}

	initialiseLocalInsight() {
		
	}

	initialiseGlobalInsight(){
		let bottomExercises = this.bottomExercises();
		let topExercises = this.topExercises();

		this._local_statistics.Statistics$ = Observable.of(topExercises,bottomExercises)
	}

	/**
	 * **Not Efficient N/E
	 * This method returns an observable bearing an adaptive view model of the top exercises
	 * @returns {Observable<AdaptiveViewModel<{}>>} 
	 * 
	 * @memberOf _StatisticsExerciseComponent
	 */
	topExercises(): Observable<AdaptiveViewModel<{}>> {
		let avm = new AdaptiveViewModel(0,{},{});
			avm.ElementName = "Top Exercises";
			avm.ElementDescription = "Shows the most performed exercises";
			
		return this.egS.topExercises()
			.flatMap(exerciseObjects=>{avm.Entitys =  exerciseObjects; return exerciseObjects})
			.map(x=>{return avm;})
	}

	bottomExercises(): Observable<AdaptiveViewModel<{}>> {
		let avm = new AdaptiveViewModel(0,{},{});
			avm.ElementName = "Bottom Exercises";
			avm.ElementDescription = "Shows the least performed exercises";
		return this.egS.bottomExercises()
			.flatMap(exerciseObjects=>{avm.Entitys =  exerciseObjects; return exerciseObjects})
			.map(x=>{return avm;})
	}

	/**Get Favourite Top3/5 Exercises */

	

	/**Get exercises listed in the goals */

	/**Most recent exercise */

	/**Top rated exercises */

	/**Least used exercises */

	/**Favourite exercises today */

	/**Favourite exercises this week */

	/**Favourite exercise this month */

}