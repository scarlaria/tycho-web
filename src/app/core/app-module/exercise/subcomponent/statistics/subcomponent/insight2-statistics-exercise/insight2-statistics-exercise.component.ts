import { Component, OnChanges, OnInit, ViewChild } from '@angular/core';

import {BaseChartDirective} from 'ng2-charts';
import {ExerciseLocalService} from '../../../../_local/exercise.local';
import {ExerciseLocalStatisticsService} from '../../../../_local/exercise.local.statistics';
import {ExerciseService} from '../../../../../../../api/app-module/exercise/service/generic/exercise/exercise.service';
import {ExerciseStatisticsService} from '../../../../../../../api/app-module/exercise/service/statistical/exercise/exercise.statistics';
import {LoggedExerciseService} from '../../../../../../../api/app-module/exercise/service/generic/logged-exercise/logged-exercise.service';
import { Observable } from 'rxjs/Rx';

@Component({
  selector: 'sub-sub-insight2-statistics-exercise',
  templateUrl: './insight2-statistics-exercise.component.html',
  styleUrls: ['./insight2-statistics-exercise.component.scss'],

})
export class Insight2StatisticsExerciseComponent  {

//   private lineChartEntity$: Observable<LineChartEntity>;
//   private lineChartEntity: Observable<LineChartEntity>;
//   private currentExercise : string;
//   private currentQueryd: QuerydLoggedExerciseEntity;
//   private colors : {}
//   private title: string;
//   private descriptor: string;

// @ViewChild( BaseChartDirective ) chart: BaseChartDirective;

//   private updateChart(){
//     console.log(this.chart);
//    this.chart.ngOnChanges({});
//   }


//   constructor(private _local: ExerciseLocalService, 
//               private _local_statistics: ExerciseLocalStatisticsService,
//               private eS: ExerciseService,
//               private esS: ExerciseStatisticsService,
//               private qdS: QuerydExerciseService,
//               private leS: LoggedExerciseService,
//               private chartS: ChartService) {

//       this.lineChartEntity$ = Observable.of(ChartService.toBasicLine([1493801988171,1493851988171,149901988171], [43,54,32], false, false));

//       // let dependent = new Array<any>();
//       // let independent = new Array<any>();
//       // let loggedExercise$ = this.leS.retrieveLoggedExerciseComplete('-gnSK54NDInon4')
// 		  // let timeLine$ = this.esS.weightBreakdownPerDay(loggedExercise$);
//       // let dataPoints$ = timeLine$.map(timeLine => {this.title = timeLine.Title; this.descriptor = timeLine.Descriptor; return timeLine.DataPoints}).flatMap(x=>x);
//       // let dataPointArr = dataPoints$.map(dataPointArr => dataPointArr)
//       // let d = dataPointArr.map(x=> {
//       //       x.forEach(dataPoint => {
//       //         dependent.push(dataPoint.Dependent);
//       //         independent.push(dataPoint.Independent);
//       //         // this.title = dataPoint.Measure;
//       //         this.lineChartEntity$ = Observable.of(ChartService.toBasicLine(independent,dependent, false, false));
//       //         this.lineChartEntity$.subscribe(x=>{this.updateChart()})
//       //       })
//       // }).subscribe();

//   }


//   ngOnChanges(){
//     this.updateChart()
//   }

  
//   onUpdate() {
//     this.updateChart();
//   }

//   clickData(){
//     this.lineChartEntity$ = this.lineChartEntity$.map(x => 
//         {x.Data.Boomerang[0]['data'][1]--;
//          x.Data.Boomerang.slice(); return Observable.of(x)}).flatMap(x=>x);
//   }

//   // dropdown buttons
//   public status: { isopen: boolean } = { isopen: false };
//     public toggleDropdown($event:MouseEvent):void {
//     $event.preventDefault();
//     $event.stopPropagation();
//     this.status.isopen = !this.status.isopen;
//   }

//   ngOnInit() {
    
//   }

//   randomize() {
//     // console.log('Hi');
//     //     this.lineChartEntity.Data.Boomerang[0]['data'] = ChartService.createRandomArray(4,0,10); 
//     // console.log('Data');
//     // console.log(this.lineChartEntity.Data)
//     // Intervals.interval(x=>{
//     //         this.lineChartEntity.Data.data = ChartService.createRandomArray(8,0,10); 
//     //         console.log('Data');
//     //         console.log(this.lineChartEntity.Data.data)
//     //     },1000,10);
//   }

//     // events
//   public chartClicked(e:any):void {
//     // console.log('Hi')
//     // this.lineChartEntity$ = this.lineChartEntity$.map(x => 
//     //     {x.Data.Boomerang[0]['data'][1]--;
//     //      x.Data.Boomerang.slice(); return Observable.of(x)}).flatMap(x=>x);
//   }

//   public chartHovered(e:any):void {
//     // console.log(e);
//   }

}