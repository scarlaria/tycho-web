import { Component, OnInit } from '@angular/core';

import {AdaptiveViewModel} from '../../../../../../../api/dynamic-library/entity/adaptive-view-model/adaptive-view-model';
import {ExerciseGlobalStatisticsService} from '../../../../../../../api/app-module/exercise/service/statistical/global-statistics/exercise-global.statistics';
import {ExerciseLocalService} from '../../../../_local/exercise.local';
import {ExerciseLocalStatisticsService} from '../../../../_local/exercise.local.statistics';
import { Observable } from 'rxjs/Rx';

@Component({
  selector: 'sub-sub-insight1-statistics-exercise',
  templateUrl: './insight1-statistics-exercise.component.html',
  styleUrls: ['./insight1-statistics-exercise.component.scss']
})
export class Insight1StatisticsExerciseComponent implements OnInit {

  private statistic: Observable<AdaptiveViewModel<{}>>;
  private statistics: Array<Observable<AdaptiveViewModel<{}>>>;
  constructor(private _local: ExerciseLocalService, private _local_statistics: ExerciseLocalStatisticsService, private egS: ExerciseGlobalStatisticsService) { 
    this.statistics = new Array<Observable<AdaptiveViewModel<{}>>>();
    this.statistic = new Observable<AdaptiveViewModel<{}>>(x=>x);

    // this._local_statistics.Statistics$.map(x => {
    //   this.statistics.push(x);
    //   //this.statistic = this.statistics[this._local.Index];
    //   console.log(this.statistic);
    //   return this.statistics
    // }).subscribe(x=>x);

  }

  ngOnInit() {
  }

  onCycleRight() {
		let newIndex = 0
		if(this.statistics.length > this._local_statistics.Index + 1){
      this._local_statistics.Index++;
		}else{
		}		
		this.statistic = this.statistics[this._local_statistics.Index];
	}

	onCycleLeft() {
		let newIndex = 0
		if(-1 < this._local_statistics.Index -1){
      this._local_statistics.Index--
		}
    console.log("NewIndex: " + this._local_statistics.Index)		
		this.statistic = this.statistics[this._local_statistics.Index];
	}

}