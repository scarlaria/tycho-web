// Angular Imports
import { NgModule } from '@angular/core';

// This Module's Components
import { LauncherComponent } from './launcher.component';

@NgModule({
    imports: [

    ],
    declarations: [
        LauncherComponent,
    ],
    exports: [
        LauncherComponent,
    ]
})
export class LauncherModule {

}
