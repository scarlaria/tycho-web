import { Observable } from 'rxjs/Rx';
import { AngularFireDatabase, FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
// import { AlertService } from '../global/alert/alert.service';
// import { AdaptiveViewModel, DB_DASH, DB_USER, GlobalService } from './../global/service/global.service';
import { Component, OnInit } from '@angular/core';
// import {AuthenticationService} from './../authentication/service/authentication.service';
import {Router} from '@angular/router';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-dashboard',
  templateUrl: './full-layout.component.html',
  // providers: [AuthenticationService]
})
export class FullLayoutComponent implements OnInit {
  private user : Observable<firebase.User>;

  constructor(private afAuth: AngularFireAuth,
              private afDB: AngularFireDatabase,
              // private aS : AuthenticationService, 
              private router: Router,
              // private alertS: AlertService
              ) {
    this.user = this.afAuth.authState;
    console.log(this.user);
  }

  // public createAlert() {
  //   let avm = new AdaptiveViewModel<string>(0, AlertService._alert_danger, '');
  //   this.alertS.add('Martin', 'Martin is sexy', '133', avm, 0, false);
  //   this.alertS.add('Omi', 'Im the best', '133', avm, 0, false);
  // }

  public disabled: boolean = false;
  public status:{isopen:boolean} = {isopen: false};

  public toggled(open:boolean):void {
    console.log('Dropdown is now: ', open);
  }

  public toggleDropdown($event:MouseEvent):void {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
  }

  getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

  onAlertClose(i : number) {
    // this.alertS.Alerts[i].onClose();
  }

  // createTempAlert() {
  //   this.createAlert();
  // }

  public logout(){
    // this.aS.logout().subscribe( x => console.log(x + ' logged out'))
  }

  ngOnInit(): void { 
  }

    get diagnostic() {
      let json = JSON.stringify(this.user, null, 2);
      return json;
    }
}
