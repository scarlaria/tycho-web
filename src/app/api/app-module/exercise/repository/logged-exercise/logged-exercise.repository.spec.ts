/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { LoggedExerciseRepository } from './logged-exercise.repository';

describe('Service: LoggedExercise.repository', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoggedExerciseRepository]
    });
  });

  it('should ...', inject([LoggedExerciseRepository], (service: LoggedExerciseRepository) => {
    expect(service).toBeTruthy();
  }));
});