import * as firebase from 'firebase/app';

import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import {DB_DASH, DB_EXERCISE} from '../../../../../globals/const/global.const';

import { AngularFireAuth } from 'angularfire2/auth';
import {ExerciseEntity} from '../../entity/exercise/exercise.entity';
import { FirebaseListFactoryOpts } from 'angularfire2/interfaces';
import {IRepository} from '../../../../../globals/interface/repository.interface';
import { Injectable } from '@angular/core';
import { NextObserver } from 'rxjs/Observer';
import {Observable} from 'rxjs/Rx';

export interface IExerciseRepository extends IRepository<ExerciseEntity>{
}

@Injectable()
export class ExerciseRepository implements IExerciseRepository {

    constructor(private afAUth: AngularFireAuth, private afDB: AngularFireDatabase) { }

    create(exercise:{}) : firebase.database.ThenableReference  {
        return this.afDB.list(DB_EXERCISE).push(exercise);
    }

    retrieveByKey(key: string): FirebaseObjectObservable<{}> {
        return this.afDB.object(DB_EXERCISE + DB_DASH + key)
    }

    retrieveByKeys(keys: Array<string>): Observable<{}[]> {
        let exercises = new Array<Observable<{}>>();
        keys.map(key => {
            exercises.push(this.afDB.object(DB_EXERCISE + DB_DASH + key))
        })
        return Observable.combineLatest(exercises)   
    }

    retrieveByKeysWithQuery(keys: Array<string>, query: FirebaseListFactoryOpts): Observable<{}[]> {
        let exercises = new Array<Observable<{}>>();
        keys.forEach(key => {
            exercises.push(this.afDB.object(DB_EXERCISE + DB_DASH + key, query))
        })
        return Observable.combineLatest(exercises);
    }

    retrieveAll(): FirebaseListObservable<{}[]> {
        return this.afDB.list(DB_EXERCISE);
    }

    retrieveAllWithQuery(query: FirebaseListFactoryOpts): FirebaseListObservable<{}[]> {
        try{
            return this.afDB.list(DB_EXERCISE, query)
        }catch(e) {
            console.log('Failed');
            return;
        }  
    }

    update(key: string, object: Object): firebase.Promise<void> {
        return this.afDB.object(DB_EXERCISE + DB_DASH + key).update(object)
    }

    updateWithPath(path: string, object: Object, key: string): firebase.Promise<void> {
        if(key) {
            return this.afDB.object(path + DB_DASH + key).update(object)
        }else{
            return this.afDB.object(path).update(object)
        }   
    }

    delete(isContextual: boolean, path: string) : firebase.Promise<void> {
        if(isContextual) {
            return this.afDB.object(DB_EXERCISE + DB_DASH + path).remove()
        } else {
            return this.afDB.object(path).remove()
        }     
    }
    
/**
     * This method ensures standardisation of exercises being uploaded into the database. It is the final
     * step that ensures data integrity and consistency before upload.
     * @param exercise 
     * @param categoryObj 
     * @param measureObj 
     * @param equipmentObj 
     * @param anatomyObj 
     * @return {} returns an object that is worthy of being uploaded to the database
     */
    purge(exercise: ExerciseEntity, 
                            categoryObj? : {}, measureObj?: {}, equipmentObj?: {},
                            anatomyObj?: {}) : {} {
        let obj = {};
        if(exercise.Anatomy) obj['anatomy'] = anatomyObj;
        if(exercise.Description) obj['description'] = exercise.Description;
        if(equipmentObj) obj['equipment'] = equipmentObj;
        if(exercise.GlobalCompletion) obj['globalCompletion'] = exercise.GlobalCompletion;
        if(exercise.Image) obj['image'] = exercise.Image;
        if(exercise.IsPrivate) obj['isPrivate'] = exercise.IsPrivate;
        if(measureObj) obj['measure'] = measureObj;
        if(exercise.Name) obj['name'] = exercise.Name;
        if(exercise.Owner) obj['owner'] = exercise.Owner;
        if(exercise.Rating) obj['rating'] = exercise.Rating;
        if(exercise.UpdatedDate) obj['updatedDate'] = exercise.UpdatedDate;
        if(categoryObj) obj['category'] = categoryObj;
        if(exercise.IsArchived) obj['isArchived']  = exercise.IsArchived;
        if(exercise.CreatedDate) obj['createdDate'] = exercise.CreatedDate;

        return obj;   
    }

}