/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ExerciseRepository } from './exercise.repository';

describe('Service: Exercise.repository', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExerciseRepository]
    });
  });

  it('should ...', inject([ExerciseRepository], (service: ExerciseRepository) => {
    expect(service).toBeTruthy();
  }));
});