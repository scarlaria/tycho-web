import { Injectable } from '@angular/core';
import {LoggedExerciseEntity} from '../../../entity/logged-exercise/logged-exercise.entity';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class ExerciseDataPointService {

//     constructor() { }

//     /**
//      * Returns a datapoint formatted for weight
//      * 
//      * @param {LoggedExerciseEntity} loggedExercise 
//      * @returns {Observable<ExerciseDataPoint[]>} 
//      * 
//      * @memberOf ExerciseDataPointService
//      */
//     createForWeight(loggedExercise: LoggedExerciseEntity): Observable<ExerciseDataPoint[]> {
//         let dataPoints = new Array<ExerciseDataPoint>();
//         loggedExercise.Set.forEach((set, index) => {
//                 // let dataPoint = new ExerciseDataPoint(set.Date, set.Weight, 'Weight', set.IsMetric);
//                 // dataPoints.push(dataPoint);
//             })
//             return Observable.of(dataPoints)
//     }

//     /**
//      * Returns a datapoint formatted for weight
//      * 
//      * @param {LoggedExerciseEntity} loggedExercise 
//      * @returns {Observable<ExerciseDataPoint[]>} 
//      * 
//      * @memberOf ExerciseDataPointService
//      */
//     createForDistance(loggedExercise: LoggedExerciseEntity): Observable<ExerciseDataPoint[]> {
//         let dataPoints = new Array<ExerciseDataPoint>();
//         loggedExercise.Set.forEach((set, index) => {
//                 // let dataPoint = new ExerciseDataPoint(set.Date, set.Distance, 'Distance', set.IsMetric);
//                 // dataPoints.push(dataPoint);
//             })
//             return Observable.of(dataPoints)
//     }

//     /**
//      * Returns a datapoint formatted for weight
//      * 
//      * @param {LoggedExerciseEntity} loggedExercise 
//      * @returns {Observable<ExerciseDataPoint[]>} 
//      * 
//      * @memberOf ExerciseDataPointService
//      */
//     createForHeight(loggedExercise: LoggedExerciseEntity): Observable<ExerciseDataPoint[]> {
//         let dataPoints = new Array<ExerciseDataPoint>();
//         loggedExercise.Set.forEach((set, index) => {
//                 //let dataPoint = new ExerciseDataPoint(set.Date, set.Height, 'Height', set.IsMetric);
//                 //dataPoints.push(dataPoint);
//             })
//             return Observable.of(dataPoints)
//     }

//     /**
//      * Returns a datapoint formatted for weight
//      * 
//      * @param {LoggedExerciseEntity} loggedExercise 
//      * @returns {Observable<ExerciseDataPoint[]>} 
//      * 
//      * @memberOf ExerciseDataPointService
//      */
//     createForReps(loggedExercise: LoggedExerciseEntity): Observable<ExerciseDataPoint[]> {
//         let dataPoints = new Array<ExerciseDataPoint>();
//         loggedExercise.Set.forEach((set, index) => {
//                 // let dataPoint = new ExerciseDataPoint(set.Date, set.Reps, 'Reps', set.IsMetric);
//                 // dataPoints.push(dataPoint);
//             })
//             return Observable.of(dataPoints)
//     }

//     /**
//      * Returns a datapoint formatted for time
//      * 
//      * @param {LoggedExerciseEntity} loggedExercise 
//      * @returns {Observable<ExerciseDataPoint[]>} 
//      * 
//      * @memberOf ExerciseDataPointService
//      */
//     createForTime(loggedExercise: LoggedExerciseEntity): Observable<ExerciseDataPoint[]> {
//         let dataPoints = new Array<ExerciseDataPoint>();
//         loggedExercise.Set.forEach((set, index) => {
//                 // let dataPoint = new ExerciseDataPoint(set.Date, set.Time, 'Time', set.IsMetric);
//                 // dataPoints.push(dataPoint);
//             })
//             return Observable.of(dataPoints)
//     }

//     create(loggedExercise: LoggedExerciseEntity): Observable<ExerciseDataPoint[]> {
//         let dataPoints = new Array<ExerciseDataPoint>();
//         loggedExercise.Set.forEach((set, index) => {
//                 // let dataPoint = new ExerciseDataPoint(set.Date, set.Time, 'Time', set.IsMetric);
//                 // dataPoints.push(dataPoint);
//             })
//             return Observable.of(dataPoints)
//     }
    
    
}