import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import {DB_DASH, DB_EXERCISE_MEASURE} from '../../../../../../../globals/const/global.const';

import { Injectable } from '@angular/core';
import {MeasureEntity} from '../../../../entity/exercise/measure/measure.entity';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class MeasureService {
    
    constructor(private afDB: AngularFireDatabase) {
        
    }

    retrieveMeasure(measureId: string): FirebaseObjectObservable<{}> {
        return this.afDB.object(DB_EXERCISE_MEASURE + DB_DASH + measureId)
    }

    retrieveMeasures(keys: Array<string>): Observable<{}[]> {
        let measures = new Array<{}>();
        keys.map(key => {
            measures.push(this.afDB.object(DB_EXERCISE_MEASURE + DB_DASH + key))
        })
        return Observable.of(measures);
    }

    retrieveMeasureAsEntity(measureId: string): Observable<MeasureEntity> {
        return this.afDB.object(DB_EXERCISE_MEASURE + DB_DASH + measureId)
                    .map(measureObject => MeasureEntity.convertObject(measureId, measureObject))
    }

    retrieveAllMeasures(): FirebaseListObservable<{}[]> {
        return this.afDB.list(DB_EXERCISE_MEASURE)
    }

    retrieveMeasures_Entity(): Observable<MeasureEntity> {
        return this.afDB.list(DB_EXERCISE_MEASURE)
                    .map(measureObject => MeasureEntity.convertObject(measureObject['$key'], measureObject))
    }

    

    /**
     * Invokes a custom {FirebaseAdapter} class to return an object from a list of string ids.
     * @param {Array<string>} measure description measure
     * @return {} description objectifies measure
     */
    // purge(measure: Array<string>) : {} {
    //     return FirebaseAdapter.prepareList(measure)
    // }

}