import * as firebase from 'firebase/app';

import {AngularFireDatabase, FirebaseListObservable} from 'angularfire2/database';
import { DB_DASH, DB_EXERCISE_EQUIPMENT } from '../../../../../../../globals/const/global.const';

import {Injectable} from '@angular/core';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class EquipmentService {
    
    constructor(private afDB: AngularFireDatabase) {
        
    }

    retrieveAllEquipments(): FirebaseListObservable<{}[]> {
        return this.afDB.list(DB_EXERCISE_EQUIPMENT)
    }

    retrieveEquipments(keys: Array<string>): Observable<{}[]> {
        let equipments = new Array<{}>();
        keys.map(key => {
            equipments.push(this.afDB.object(DB_EXERCISE_EQUIPMENT + DB_DASH + key))
        })
        return Observable.of(equipments);
    }    

    /**
     * Invokes a custom {FirebaseAdapter} class to return an object from a list of string ids.
     * @param {Array<string>} equipment description equipment
     * @return {} description objectifies equipment
     */
    purge(equipment: Array<string>) : {} {
        return //FirebaseAdapter.prepareList(equipment)
    }
 
    /**
     * Retrieves a single category from the @exercise-equipment table
     * @param key 
     * @returns Reference
     */
    retrieveEquipment(key : string) : firebase.database.Reference {
       return this.afDB.object(DB_EXERCISE_EQUIPMENT + DB_DASH + key).$ref;
    }
}