import * as firebase from 'firebase/app';

import {
    AngularFireDatabase,
    FirebaseListFactory,
    FirebaseListObservable,
    FirebaseObjectObservable,
} from 'angularfire2/database';
import { AsyncSubject, Observable, Subject } from 'rxjs/Rx';
import {DB_DASH, DB_EXERCISE} from '../../../../../../globals/const/global.const';
import { FirebaseListFactoryOpts, Query } from 'angularfire2/interfaces';

import { AngularFireAuth } from 'angularfire2/auth';
import {CategoryService} from './category/category.service';
import {EquipmentService} from './equipment/equipment.service';
import {ExerciseEntity} from '../../../entity/exercise/exercise.entity';
import { ExerciseRepository } from '../../../repository/exercise/exercise.repository';
import {GlobalService} from '../../../../../dynamic-library/service/global/global.service';
import { GlobalSettingsService } from '../../../../../../globals/service/settings/global-settings.service';
import { IService } from '../../../../../../globals/interface/service.interface';
import {Injectable} from '@angular/core';
import {MeasureService} from './measure/measure.service';
import { NextObserver } from 'rxjs/Observer';

export interface IExerciseService extends IService<ExerciseEntity> {
    retrieveAllExercisesWithQueryAsEntity(query: FirebaseListFactoryOpts): Observable<ExerciseEntity[]>;
    retrieveAllExercisesWithQueryAsEntity_Stream?(query: FirebaseListFactoryOpts): Observable<ExerciseEntity>;
    retrieveAllExercisesAsEntity(): Observable<ExerciseEntity[]>;
    retrieveAllExercisesAsEntity_Stream?(): Observable<ExerciseEntity>;
    retrieveAllExercisesWithQuery(query: FirebaseListFactoryOpts): FirebaseListObservable<{}[]>;
    retrieveAllExercises(): FirebaseListObservable<{}[]>;
    retrieveExerciseAsEntity(key: string): Observable<ExerciseEntity>;
    retrieveExercise(key: string): FirebaseObjectObservable<{}>;
    retrieveExercisesAsEntity(keys: Array<string>): Observable<ExerciseEntity[]>
    retrieveExercisesAsEntity_Stream?(keys: Array<string>): Observable<ExerciseEntity>;
    retrieveExercises(keys: Array<string>): Observable<{}[]>;
    retrieveExercisesWithQueryAsEntity(keys: Array<string>, query: FirebaseListFactoryOpts): Observable<ExerciseEntity[]>;
    retrieveExercisesWithQueryAsEntity_Stream?(keys: Array<string>, query: FirebaseListFactoryOpts): Observable<ExerciseEntity>;
    retrieveExercisesWithQuery(keys: Array<string>, query: FirebaseListFactoryOpts): Observable<{}[]>;
}

@Injectable()
export class ExerciseService implements IExerciseService {

    constructor(private cS : CategoryService, private mS : MeasureService, private eqS : EquipmentService,
    private afDB : AngularFireDatabase, private afAuth: AngularFireAuth, private eR: ExerciseRepository) { }
    
    /**
     * Performs a serch on an Observable array based on an exercise objects searchField
     * 
     * @param {Observable<{}[]>} exercises$ 
     * @param {string} searchField the field 'name', 'date', 'description'
     * @param {string} searchItem The element you are looking for 'dumbell flyes', 'bench press'
     * @returns {Observable<{}>} 
     * 
     * @memberof ExerciseService
     */
    search(exercises$: Observable<{}[]>, searchField:string, searchItem: string): Observable<{}>{
        return exercises$.map(exerciseArray => 
                Observable.from(exerciseArray)
                          .filter(exercise => GlobalService.search(searchItem, exercise[searchField])))
                          .flatMap(x=>x)
    }
                                
                                
                                /**DATABASE RELATED EXERCISE FUNCTIONS */

    /**
     * This method pushes a created {ExerciseEntity} to the database and returns a reference of the push. The key can then be obtained
     * @param exercise 
     * @return {ThenableReference}
     */
    create(exercise : ExerciseEntity) : firebase.database.ThenableReference {
        return this.afDB.list(DB_EXERCISE).push(exercise);
    }

    /**
     * Retrieves all exercises as entities based on a query
     * 
     * @param {FirebaseListFactoryOpts} query 
     * @returns {Observable<ExerciseEntity[]>} 
     * 
     * @memberof ExerciseService
     */
    retrieveAllExercisesWithQueryAsEntity(query: FirebaseListFactoryOpts): Observable<ExerciseEntity[]> {
        return this.eR.retrieveAllWithQuery(query)
            .switchMap(exerciseObjects => Observable.from(exerciseObjects))
                        .map(exerciseObj => new Array<ExerciseEntity>()
                            .concat(ExerciseEntity.convertObject(exerciseObj['$key'], exerciseObj)));
    }

    /**
     * Retrievews a stream of exercise entities based on a query
     * 
     * @param {FirebaseListFactoryOpts} query 
     * @returns {Observable<ExerciseEntity>} 
     * 
     * @memberof ExerciseService
     */
    retrieveAllExercisesWithQueryAsEntity_Stream(query: FirebaseListFactoryOpts): Observable<ExerciseEntity> {
        return this.eR.retrieveAllWithQuery(query)
            .switchMap(exerciseObjects => Observable.from(exerciseObjects))
                        .map(exerciseObj => ExerciseEntity.convertObject(exerciseObj['$key'], exerciseObj));
    }
    
    /**
     * Retrieves all the exercises as entities
     * 
     * @returns {Observable<ExerciseEntity[]>} 
     * 
     * @memberof ExerciseService
     */
    retrieveAllExercisesAsEntity(): Observable<ExerciseEntity[]> {
        return this.eR.retrieveAll()
            .switchMap(exerciseObjects => Observable.from(exerciseObjects))
                    .map(exerciseObj => new Array<ExerciseEntity>()
                        .concat(ExerciseEntity.convertObject(exerciseObj['$key'], exerciseObj)));
    }
    /**
     * Retrieves All the exercies as a stream f exercise entities
     * 
     * @returns {Observable<ExerciseEntity>} 
     * 
     * @memberof ExerciseService
     */
    retrieveAllExercisesAsEntity_Stream?(): Observable<ExerciseEntity> {
        return this.eR.retrieveAll()
            .switchMap(exerciseObjects => Observable.from(exerciseObjects))
                    .map(exerciseObj => ExerciseEntity.convertObject(exerciseObj['$key'], exerciseObj));
    }

    /**
     * Retrieves all exercises based on a query
     * 
     * @param {FirebaseListFactoryOpts} query 
     * @returns {FirebaseListObservable<{}[]>} 
     * 
     * @memberof ExerciseService
     */
    retrieveAllExercisesWithQuery(query: FirebaseListFactoryOpts): FirebaseListObservable<{}[]> {
        return this.eR.retrieveAllWithQuery(query)
    }

    /**
     * Retrieves all exercises
     * 
     * @returns {FirebaseListObservable<{}[]>} 
     * 
     * @memberof ExerciseService
     */
    retrieveAllExercises(): FirebaseListObservable<{}[]> {
        return this.eR.retrieveAll()
    }

    /**
     * Retrieves an exercise as an entity based on a key
     * 
     * @param {string} key 
     * @returns {Observable<ExerciseEntity>} 
     * 
     * @memberof ExerciseService
     */
    retrieveExerciseAsEntity(key: string): Observable<ExerciseEntity> {
        return this.eR.retrieveByKey(key)
            .map(exerciseObj => ExerciseEntity.convertObject(exerciseObj['$key'], exerciseObj));
    }

    /**
     * Retrieves an exercise based on a key
     * 
     * @param {string} key 
     * @returns {FirebaseObjectObservable<{}>} 
     * 
     * @memberof ExerciseService
     */
    retrieveExercise(key: string): FirebaseObjectObservable<{}> {
        return this.eR.retrieveByKey(key);
    }

    /**
     * Retrieves exercises as entities based on an array of exercises
     * 
     * @param {Array<string>} keys 
     * @returns {Observable<ExerciseEntity[]>} 
     * 
     * @memberof ExerciseService
     */
    retrieveExercisesAsEntity(keys: Array<string>): Observable<ExerciseEntity[]> {
        return this.eR.retrieveByKeys(keys)
            .switchMap(exerciseObjects => Observable.from(exerciseObjects))
                .map(exerciseObj => new Array<ExerciseEntity>()
                    .concat(ExerciseEntity.convertObject(exerciseObj['$key'], exerciseObj)));
    }

    /**
     * Retrieves a stream of exercises as entities based on an array of exercises 
     * 
     * @param {Array<string>} keys 
     * @returns {Observable<ExerciseEntity>} 
     * 
     * @memberof ExerciseService
     */
    retrieveExercisesAsEntity_Stream(keys: Array<string>): Observable<ExerciseEntity> {
        return this.eR.retrieveByKeys(keys)
            .switchMap(exerciseObjects => Observable.from(exerciseObjects))
                .map(exerciseObj => ExerciseEntity.convertObject(exerciseObj['$key'], exerciseObj));
    }

    /**
     * Retrievess exercises based on an array of exercies
     * 
     * @param {Array<string>} keys 
     * @returns {Observable<{}[]>} 
     * 
     * @memberof ExerciseService
     */
    retrieveExercises(keys: Array<string>): Observable<{}[]> {
        return this.eR.retrieveByKeys(keys)
    }

    /**
     * Retrieves exercises as entities based on an array of exercises filtered by query
     * 
     * @param {Array<string>} keys 
     * @param {FirebaseListFactoryOpts} query 
     * @returns {Observable<ExerciseEntity[]>} 
     * 
     * @memberof ExerciseService
     */
    retrieveExercisesWithQueryAsEntity(keys: Array<string>, query: FirebaseListFactoryOpts): Observable<ExerciseEntity[]> {
        return this.eR.retrieveByKeysWithQuery(keys, query) 
            .switchMap(exerciseObjects => Observable.from(exerciseObjects)
                .map(exerciseObj => new Array<ExerciseEntity>()
                    .concat(ExerciseEntity.convertObject(exerciseObj['$key'], exerciseObj))));
    }
    
    /**
     * Retrieves a stream of exercises as entities based on an array of exercises filtered by query
     * 
     * @param {Array<string>} keys 
     * @param {FirebaseListFactoryOpts} query 
     * @returns {Observable<ExerciseEntity>} 
     * 
     * @memberof ExerciseService
     */
    retrieveExercisesWithQueryAsEntity_Stream?(keys: Array<string>, query: FirebaseListFactoryOpts): Observable<ExerciseEntity> {
        return this.eR.retrieveByKeysWithQuery(keys, query) 
            .switchMap(exerciseObjects => Observable.from(exerciseObjects)
                .map(exerciseObj => ExerciseEntity.convertObject(exerciseObj['$key'], exerciseObj)));
    }
    
    /**
     * Retrieves exercises based on an array of exercises filtered by query
     * 
     * @param {Array<string>} keys 
     * @param {FirebaseListFactoryOpts} query 
     * @returns {Observable<{}[]>} 
     * 
     * @memberof ExerciseService
     */
    retrieveExercisesWithQuery(keys: Array<string>, query: FirebaseListFactoryOpts): Observable<{}[]> {
        return this.eR.retrieveByKeysWithQuery(keys, query)     
    }
    /**
     * Returns a throttles number of exercise objects based on the limit
     * 
     * @param {number} [limit=GlobalSettingsService.EXERCISE_LIMIT_TO_FIRST] 
     * @returns {FirebaseListObservable<{}[]>} 
     * 
     * @memberOf ExerciseService
     */    
    retrieveExercisesThrottled(limit:number = GlobalSettingsService.EXERCISE_LIMIT_TO_FIRST): Observable<{}[]> {
            let query = <FirebaseListFactoryOpts>{}
    
            query.query = {};
            query.query.limitToFirst = limit
            console.log(query);
            return this.eR.retrieveAllWithQuery(query.query.limitToFirst)
                        .map(object => new Array<ExerciseEntity>().concat(object['$key']))
    }

    /**
     * Returns a throttles number of exercise objects based on the limit
     * 
     * @param {number} [limit=GlobalSettingsService.EXERCISE_LIMIT_TO_FIRST] 
     * @returns {FirebaseListObservable<{}[]>} 
     * 
     * @memberOf ExerciseService
     */
    retrieveExercisesAsEntityThrottled(limit:number = GlobalSettingsService.EXERCISE_LIMIT_TO_FIRST): Observable<ExerciseEntity[]> {
        let query = <FirebaseListFactoryOpts>{}
        query.query.limitToFirst = limit
        return this.eR.retrieveAllWithQuery(query)
                    .map(object => new Array<ExerciseEntity>().concat(ExerciseEntity.convertObject(object['$key'],object)))
    }

    /**
     * Performs a simpleconversion to convert an observable exercise object to an observable exercise entity
     * 
     * @param {Observable<{}>} object$ 
     * @returns {Observable<ExerciseEntity>} 
     * 
     * @memberof ExerciseService
     */
    convert(object$: Observable<{}>): Observable<ExerciseEntity> {
        return object$.map(object => ExerciseEntity.convertObject(object['$key'], object))     
    }

    /**
     * Updates an exercise in the main branch of the exercise tree
     * 
     * @param {string} key 
     * @param {ExerciseEntity} exercise 
     * @returns 
     * 
     * @memberof ExerciseService
     */
    update(key : string, exercise :ExerciseEntity | {}){
        let purge: {}
        if(exercise instanceof ExerciseEntity) {
            purge = this.eR.purge(exercise);
        }else{
            purge = exercise;
        }    
        return this.eR.update(key, purge)
    }


    updateWithPath(path: string, exercise :ExerciseEntity | {}, key? : string) {
        let purge: {}
        if(exercise instanceof ExerciseEntity) {
            purge = this.eR.purge(exercise);
        }else{
            purge = exercise;
        }     
        return this.eR.updateWithPath(path, purge, key)
    }

    /**
     * Removes an element from the database
     * @param id 
     */
    delete(id : string): firebase.Promise<void> {
        return this.eR.delete(true, '')
    }
}


