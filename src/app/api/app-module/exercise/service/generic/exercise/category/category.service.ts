import * as firebase from 'firebase/app';

import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { DB_DASH, DB_EXERCISE_CATEGORY } from '../../../../../../../globals/const/global.const';

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class CategoryService {
    
    constructor(private afDB: AngularFireDatabase) {
        
    }

    retrieveAllCategorys(): FirebaseListObservable<{}[]> {
        return this.afDB.list(DB_EXERCISE_CATEGORY)
    }

    retrieveCategorys(keys: Array<string>): Observable<{}[]> {
        let categorys = new Array<{}>();
        keys.map(key => {
            categorys.push(this.afDB.object(DB_EXERCISE_CATEGORY + DB_DASH + key))
        })
        return Observable.of(categorys);
    }

    

    /**
     * Invokes a custom {FirebaseAdapter} class to return an object from a list of string ids.
     * @param {Array<string>} category description category
     * @return {} description objectifies category
     */
    purge(category: Array<string>) : {} {
        return //FirebaseAdapter.prepareList(category)
    }

    /**
     * Retrieves a single category from the @exercise-category table
     * @param key 
     * @returns Reference
     */
    retrieveCategory(key : string) : FirebaseObjectObservable<{}> {
       return this.afDB.object(DB_EXERCISE_CATEGORY + DB_DASH + key)
    }
}