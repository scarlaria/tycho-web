import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import {DB_DASH, DB_LOGGED_EXERCISE, DB_LOGGED_EXERCISE_BY_LOGGED_WORKOUT, DB_LOGGED_EXERCISE_BY_USER} from '../../../../../../globals/const/global.const';
import { Observable, Observer, Subject } from 'rxjs/Rx';

import { AngularFireAuth } from 'angularfire2/auth';
import {ExerciseService} from '../exercise/exercise.service';
import { Injectable } from '@angular/core';
import {LoggedExerciseEntity} from '../../../entity/logged-exercise/logged-exercise.entity';
import { NextObserver } from 'rxjs/Observer';

@Injectable()
export class LoggedExerciseService {

    constructor(private afDB: AngularFireDatabase, private afAuth: AngularFireAuth, private eS: ExerciseService) { }

    /**
     * Returns a list of entity-complete logged exercises based on an array of string ids
     * If no paramter, then it returns all the exercises belonging to a user
     * 
     * @param {Array<string>} loggedExerciseIds 
     * @returns {Observable<LoggedExerciseEntity>} 
     * 
     * @memberOf LoggedExerciseService
     */
    retrieveLoggedExercisesComplete(loggedExerciseIds? : Array<string>): Observable<LoggedExerciseEntity>
    {
        if(loggedExerciseIds) {
            return Observable.from(loggedExerciseIds)
                .map(loggedExerciseId => this.retrieveLoggedExerciseComplete(loggedExerciseId))
                .flatMap(x=>x);
        } else {
            return this.afAuth.authState.map(user=> this.retrieveLoggedExercisesKeys(user.uid)
                        .mergeMap(key => this.retrieveLoggedExerciseComplete(key)))
                        .flatMap(x => x);
        }
    }

    /**
     * Returns a single entity-complete logged exercise based on a string id
     * 
     * @param {string} loggedExerciseId 
     * @returns {Observable<LoggedExerciseEntity>} 
     * 
     * @memberOf LoggedExerciseService
     */
    retrieveLoggedExerciseComplete(loggedExerciseId : string) : Observable<LoggedExerciseEntity> {
        return this.afDB.object(DB_LOGGED_EXERCISE + DB_DASH + loggedExerciseId)
                .map(loggedExerciseObject => {
                    let trackedExercise = <string>Object.keys(loggedExerciseObject)[0];
                    let loggedExercise =LoggedExerciseEntity.convertObject(loggedExerciseId, 
                                                        loggedExerciseObject, 
                                                        trackedExercise,
                                                        loggedExerciseObject[loggedExerciseId])
                    let exercise$ = this.eS.retrieveExercise(trackedExercise)
                    loggedExercise.Exercise$ = exercise$;
                    return loggedExercise;
                })                   
    }
    
    /**
     * Returns a logged exercise based on a look up id as an observable.
     * @param id 
     * @return {Observable<LoggedExerciseEntity>} description
     */
    retrieveLoggedExercise(id: string): Observable<{}> {
        return this.afDB.object(DB_LOGGED_EXERCISE + DB_DASH + id);
    }

    /**
     * Returns an array of logged-exercise-keys
     * 
     * @param {string} userId 
     * @returns {Observable<{}>} 
     * 
     * @memberOf LoggedExerciseService
     */
    retrieveLoggedExercisesKeys(userId?: string) : Observable<string> {
        if(userId != null || userId != undefined) {
            return this.afDB.list(DB_LOGGED_EXERCISE_BY_USER + DB_DASH + userId)
                        .mergeMap(elem => Observable.from(elem).map(x=>x['$key']))
        }else {
            return this.afAuth.authState.map(user => this.retrieveKeys(user.uid))
                        .flatMap(x=>x)
        }
    }

    /**
     * Helper method to assist in retrieving keys
     * 
     * @private
     * @param {string} userId 
     * @returns 
     * 
     * @memberOf LoggedExerciseService
     */
    retrieveKeys(userId: string): Observable<{}> {
        return this.afDB.list(DB_LOGGED_EXERCISE_BY_USER + DB_DASH + userId)
                .map(elem => elem.map(t => t['$key']))
                .flatMap(x=>x)
    }


    /**
     * Returns an object that has the key {LoggedExercise} : Value {ExerciseEntity}
     * 
     * @param {string} userId 
     * 
     * @memberOf LoggedExerciseService
     */
     retrieveLoggedExerciseList(userId: string): Observable<{}> {
        return this.afDB.list(DB_LOGGED_EXERCISE_BY_USER + DB_DASH + userId)
            .flatMap(x=>x)   
     }

    /**
     * # Incomplete NF
     * Return corresponding logged-workout
     * 
     * @param {string} loggedExerciseId 
     * @returns {Observable<string>} 
     * 
     * @memberOf LoggedExerciseService
     */
    retrieveLoggedWorkout(loggedExerciseId: string): Observable<string>{
    return this.afDB.list(DB_LOGGED_EXERCISE_BY_LOGGED_WORKOUT, {
                query: {
                    orderByChild: loggedExerciseId,
                    equalTo : loggedExerciseId
                }
            }).flatMap(x=>x)
    }

    /**
     * Retrieves an array of LoggedExercise objects from the loggedUser as they appear in the database.
     * 
     * @returns {Observable<any[]>} 
     * 
     * @memberOf ExerciseService
     */
    retrieveLoggedExercises(): Observable<LoggedExerciseEntity> {
        return this.afAuth.authState.map(user=> this.retrieveLoggedExercisesKeys(user.uid)
                                        .mergeMap(key => this.retrieveLoggedExercise(key)))
                                        .flatMap(x => x);
    }

    retrieveLoggedExercisesList(): Observable<{}> {
        return this.afAuth.authState.map(user => this.retrieveLoggedExerciseList(user.uid))
                .flatMap(x=>x)
    }

    retrieveLoggedExercisesByUser(): Observable<{}[]>{
        return this.afAuth.authState.map(user => this.afDB.list(DB_LOGGED_EXERCISE_BY_USER + DB_DASH + user.uid))
                .flatMap(x=>x);
    }

    /**
     * Retrieves a list of key-value pairs of logged-exercise - exercise keys
     * 
     * @param {string} exerciseId 
     * @returns {Observable<{}[]>} 
     * 
     * @memberOf LoggedExerciseService
     */
    retrieveByExerciseQuery(exerciseId: string): Observable<{}[]> {
        return this.afAuth.authState.map(user => this.afDB.list(DB_LOGGED_EXERCISE_BY_USER + DB_DASH + user.uid, 
                {
                    query: {
                        orderByChild: exerciseId,
                        equalTo: exerciseId
                    }
                }
                )).flatMap(x=>x);
        }

    /**
     * Retrieves a stream of logged-exercises based on the exercise one is searching for.
     * 
     * @param {string} exerciseId 
     * @returns {Observable<LoggedExerciseEntity>} 
     * 
     * @memberOf LoggedExerciseService
     */
    retrieveByExerciseQueryComplete(exerciseId: string): Observable<LoggedExerciseEntity> {
        return this.retrieveByExerciseQuery(exerciseId).map(loggedObjects => {console.log(loggedObjects); return Observable.from(loggedObjects)
                    .map(loggedObject => {console.log(loggedObject); return this.retrieveLoggedExerciseComplete(loggedObject['$key'])})
                    .flatMap(x=>x)})
                    .flatMap(x=>x);
    }

    update(loggedExercise: LoggedExerciseEntity): boolean {
        return false;
    }

    delete(loggedExerciseId: string): boolean {
        return false;
    }
  
}