import {AngularFireDatabase} from 'angularfire2/database';
import {DB_EXERCISE} from '../../../../../../globals/const/global.const';
import {ExerciseService} from '../../generic/exercise/exercise.service';
import { Injectable } from '@angular/core';
import {LoggedExerciseService} from '../../generic/logged-exercise/logged-exercise.service';
import { Observable } from 'rxjs/Rx';

export interface IExerciseGlobal {
    topExercises
    bottomExercises
    totalCompletedExercises
    topExercisesToday
    topExercisesWeek
    topExercisesMonth
}

@Injectable()
export class ExerciseGlobalStatisticsService implements IExerciseGlobal {

    constructor(private leS: LoggedExerciseService, private eS: ExerciseService, private afDB: AngularFireDatabase) { }

    topExercises(limit: number = 5): Observable<{}[]> {
        return this.afDB.list(DB_EXERCISE, {query:{orderByChild: 'globalCompletion', limitToLast: limit}})
    }

    bottomExercises(limit: number = 5): Observable<{}[]> {
        return this.afDB.list(DB_EXERCISE, {query:{orderByChild: 'globalCompletion', limitToFirst: limit}})
    }

    totalCompletedExercises(): Observable<number>{
        /**Should be performed by firebase functions */
        return null;
    }

    topExercisesToday(): Observable<{}> {
        /**Should be performed by firebase functions */
        return null
    }

    topExercisesWeek(): Observable<{}> {
        /**Should be performed by firebase functions */
        return null
    }

    topExercisesMonth(): Observable<{}> {
        /**Should be performed by firebase functions */
        return null
    }    
}