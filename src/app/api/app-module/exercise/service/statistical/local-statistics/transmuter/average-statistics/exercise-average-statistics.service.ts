// import {TransmutedLoggedExerciseAverage} from '../../../../../entity/transmuted-logged-exercise/average/transmuted-logged-exercise-average';
// import {LoggedExerciseEntity} from '../../../../../entity/logged-exercise.entity';
// import {Injectable} from '@angular/core';
// import {SetService} from '../../../../generic/set-service/set.service';
// import {MEASURE_WEIGHT, GlobalService, MEASURE_HEIGHT, MEASURE_DISTANCE, MEASURE_TIME, MEASURE_REPS} from '../../../../../../global/service/global.service';


// export interface IExerciseAverageStatistics {
//     weightBreakdown(loggedExercise : LoggedExerciseEntity) : TransmutedLoggedExerciseAverage
//     heightBreakdown(loggedExercise : LoggedExerciseEntity) : TransmutedLoggedExerciseAverage
//     repsBreakdown(loggedExercise : LoggedExerciseEntity) : TransmutedLoggedExerciseAverage
//     timeBreakdown(loggedExercise : LoggedExerciseEntity) : TransmutedLoggedExerciseAverage
//     distanceBreakdown(loggedExercise : LoggedExerciseEntity) : TransmutedLoggedExerciseAverage
// }

// export interface IAverageMeasure {
//     averageWeight
//     averageHeight
//     averageDistance
//     averageTime
//     averageReps
// }

// /**
//  * Responsible for delivering all averages for any given measure from a logged-exercise
//  * @author info@scarlaria.com
//  * @export
//  * @class ExerciseAverageStatisticsService
//  * @implements {IExerciseAverageStatistics}
//  * @implements {IAverageMeasure}
//  */
// @Injectable()
// export class ExerciseAverageStatisticsService implements IExerciseAverageStatistics,
// IAverageMeasure {

//     constructor(private sS : SetService) {}

//     /**
//      * Returns the average weight for a group of sets;
//      *
//      * @param {LoggedExerciseEntity} loggedExercise
//      * @returns {number}
//      *
//      * @memberOf ExerciseAverageStatisticsService
//      */
//     averageWeight(loggedExercise : LoggedExerciseEntity) : number {
//         let purgedSets = this.sS.purgeSet(loggedExercise, MEASURE_WEIGHT);
//         return GlobalService.getAverage(purgedSets);
//     }
//     /**
//      * Returns the average height for a group of sets;
//      *
//      * @param {LoggedExerciseEntity} loggedExercise
//      * @returns {number}
//      *
//      * @memberOf ExerciseAverageStatisticsService
//      */
//     averageHeight(loggedExercise : LoggedExerciseEntity) : number {
//         let purgedSets = this.sS.purgeSet(loggedExercise, MEASURE_HEIGHT);
//         return GlobalService.getAverage(purgedSets);
//     }
//     /**
//      * Returns the average distance for a group of sets;
//      *
//      * @param {LoggedExerciseEntity} loggedExercise
//      * @returns {number}
//      *
//      * @memberOf ExerciseAverageStatisticsService
//      */
//     averageDistance(loggedExercise : LoggedExerciseEntity) : number {
//         let purgedSets = this.sS.purgeSet(loggedExercise, MEASURE_DISTANCE);
//         return GlobalService.getAverage(purgedSets);
//     }
//     /**
//      * Returns the average time for a group of sets;
//      *
//      * @param {LoggedExerciseEntity} loggedExercise
//      * @returns {number}
//      *
//      * @memberOf ExerciseAverageStatisticsService
//      */
//     averageTime(loggedExercise : LoggedExerciseEntity) : number {
//         let purgedSets = this.sS.purgeSet(loggedExercise, MEASURE_TIME);
//         return GlobalService.getAverage(purgedSets);
//     }

//     /**
//      * Returns the average reps for a group of sets;
//      *
//      * @param {LoggedExerciseEntity} loggedExercise
//      * @returns {number}
//      *
//      * @memberOf ExerciseAverageStatisticsService
//      */
//     averageReps(loggedExercise : LoggedExerciseEntity) : number {
//         let purgedSets = this.sS.purgeSet(loggedExercise, MEASURE_REPS);
//         return GlobalService.getAverage(purgedSets);
//     }

//     /**
//      * Retrieves the average weight for a user for a given loggedExercise
//      *
//      * @param {LoggedExerciseEntity} loggedExercise
//      * @returns {TransmutedLoggedExerciseAverage}
//      *
//      * @memberOf ExerciseAverageStatisticsService
//      */
//     public weightBreakdown(loggedExercise : LoggedExerciseEntity) : TransmutedLoggedExerciseAverage {
//         let tleA: TransmutedLoggedExerciseAverage;
//         let average = this.averageWeight(loggedExercise);
//         tleA = new TransmutedLoggedExerciseAverage(loggedExercise);
//         tleA.Measure = MEASURE_WEIGHT;
//         tleA.AverageValue = average;
//         return tleA;
//     }

//     /**
//      * Retrieves the average height for a user for a given loggedExercise
//      *
//      * @param {LoggedExerciseEntity} loggedExercise
//      * @returns {TransmutedLoggedExerciseAverage}
//      *
//      * @memberOf ExerciseAverageStatisticsService
//      */
//     public heightBreakdown(loggedExercise : LoggedExerciseEntity) : TransmutedLoggedExerciseAverage {
//         let tleA: TransmutedLoggedExerciseAverage;
//         let average = this.averageHeight(loggedExercise);
//         tleA = new TransmutedLoggedExerciseAverage(loggedExercise);
//         tleA.Measure = MEASURE_HEIGHT;
//         tleA.AverageValue = average;
//         return tleA;
//     }

//     /**
//      * Retrieves the average reps for a user for a given loggedExercise
//      *
//      * @param {LoggedExerciseEntity} loggedExercise
//      * @returns {TransmutedLoggedExerciseAverage}
//      *
//      * @memberOf ExerciseAverageStatisticsService
//      */
//     public repsBreakdown(loggedExercise : LoggedExerciseEntity) : TransmutedLoggedExerciseAverage {
//         let tleA: TransmutedLoggedExerciseAverage;
//         let average = this.averageReps(loggedExercise);
//         tleA = new TransmutedLoggedExerciseAverage(loggedExercise);
//         tleA.Measure = MEASURE_REPS;
//         tleA.AverageValue = average;
//         return tleA;
//     }

//     /**
//      * Retrieves the average time for a user for a given loggedExercise
//      *
//      * @param {LoggedExerciseEntity} loggedExercise
//      * @returns {TransmutedLoggedExerciseAverage}
//      *
//      * @memberOf ExerciseAverageStatisticsService
//      */
//     public timeBreakdown(loggedExercise : LoggedExerciseEntity) : TransmutedLoggedExerciseAverage {
//         let tleA: TransmutedLoggedExerciseAverage;
//         let average = this.averageTime(loggedExercise);
//         tleA = new TransmutedLoggedExerciseAverage(loggedExercise);
//         tleA.Measure = MEASURE_TIME;
//         tleA.AverageValue = average;
//         return tleA;
//     }

//     /**
//      * Retrieves the average distance for a user for a given loggedExercise
//      *
//      * @param {LoggedExerciseEntity} loggedExercise
//      * @returns {TransmutedLoggedExerciseAverage}
//      *
//      * @memberOf ExerciseAverageStatisticsService
//      */
//     public distanceBreakdown(loggedExercise : LoggedExerciseEntity) : TransmutedLoggedExerciseAverage {
//         let tleA: TransmutedLoggedExerciseAverage;
//         let average = this.averageDistance(loggedExercise);
//         tleA = new TransmutedLoggedExerciseAverage(loggedExercise);
//         tleA.Measure = MEASURE_DISTANCE;
//         tleA.AverageValue = average;
//         return tleA;
//     }

// }

// //octkp qodwtc