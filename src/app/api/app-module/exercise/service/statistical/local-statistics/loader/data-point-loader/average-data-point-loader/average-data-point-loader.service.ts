// import { DependentYEntity } from '../../../../../../../chart/entity/axes/dependent-y/dependent-y.entity';
// import { IndependentXEntity } from '../../../../../../../chart/entity/axes/independent-x/independent-x.entity';
// import {
//     AverageDataPointEntity
// } from '../../../../../../entity/data-point/average-data-point/average-data-point.entity';
// import { ExerciseDataPoint } from '../../../../../../entity/exercise-data-point';
// import {
//     TransmutedLoggedExerciseAverage
// } from '../../../../../../entity/transmuted-logged-exercise/average/transmuted-logged-exercise-average';
// import { IDataPointLoader } from '../idata-point-loader.interface';
// import { Injectable } from '@angular/core';

// export interface IAverageDataPointLoader extends IDataPointLoader {

// }

// /**
//  * Responsible for creating datapoints based on the {TransmutedLoggedExerciseAverag}
//  * @author info@scarlaria.com
//  * @export
//  * @class AverageDataPointLoaderService
//  * @implements {IDataPointLoader}
//  */
// @Injectable()
// export class AverageDataPointLoaderService implements IDataPointLoader {

//     constructor() { }

//     /**
//      * Returns a data-point representing a single point an a graph
//      * 
//      * 
//      * @memberOf AverageDataPointLoaderService
//      */
//     public toDataPoint = function(independentX: TransmutedLoggedExerciseAverage, dependentY: TransmutedLoggedExerciseAverage): AverageDataPointEntity{   
//         let x = new IndependentXEntity(independentX.AverageValue, independentX.Measure);
//         let y = new DependentYEntity(dependentY.AverageValue, dependentY.Measure);
//         let dataPoint = new ExerciseDataPoint(y,x,dependentY.Measure,independentX.Measure,dependentY.LoggedExercise);
//         return dataPoint;
//     }

//     // public toDataPoint2 = function(dependentY: DependentYEntity, independentX: IndependentXEntity): AverageDataPointEntity{   
//     //     let dataPoint = new ExerciseDataPoint(dependentY..AverageValue,independentX.AverageValue,dependentY.Measure, independentX.Measure);
//     //     dataPoint.LoggedExercise = dependentY.LoggedExercise;
//     //     return dataPoint;
//     // }

//     public toDateDataPoint = function(independentX: TransmutedLoggedExerciseAverage, dependentY: TransmutedLoggedExerciseAverage): AverageDataPointEntity{   
//         let x = new IndependentXEntity(independentX.AverageValue, independentX.Measure);
//         let y = new DependentYEntity(dependentY.AverageValue, dependentY.Measure);
//         let dataPoint = new ExerciseDataPoint(y,x,dependentY.Measure,independentX.Measure,dependentY.LoggedExercise);
//         return dataPoint;
//     }
// }