// import { DependentYEntity } from '../../../../../../../chart/entity/axes/dependent-y/dependent-y.entity';
// import { IndependentXEntity } from '../../../../../../../chart/entity/axes/independent-x/independent-x.entity';
// import {
//     MeraclusDataPointEntity
// } from '../../../../../../entity/data-point/meraclus-data-point/meraclus-data-point.entity';
// import {ExerciseDataPoint} from '../../../../../../entity/exercise-data-point';
// import {
//     TransmutedLoggedExerciseMeraclusEntity
// } from '../../../../../../entity/transmuted-logged-exercise/meraclus/transmuted-logged-exercise-meraclus.entity';
// import { IDataPointLoader } from '../idata-point-loader.interface';
// import {Injectable} from '@angular/core';

// export interface IMeraclusDataPointLoader extends IDataPointLoader {

// }

// @Injectable()
// export class MeraclusDataPointLoaderService implements IMeraclusDataPointLoader {
//     constructor() { }

//     /**
//      * Returns a data-point representing a single point an a graph
//      * 
//      * 
//      * @memberOf MeraclusDataPointLoaderService
//      */
//     public toDataPoint = function(tleM: TransmutedLoggedExerciseMeraclusEntity, xMeasure: string, yMeasure: string, independentAxisValue: number): MeraclusDataPointEntity[]{   
//         let dataPoints = new Array<ExerciseDataPoint>();
//         tleM.LoggedExercise.Set.forEach((set,index) => {
//             let x = new IndependentXEntity(0, '');
//             let y = new DependentYEntity(0, '');
//             let dataPoint = new ExerciseDataPoint(y,x, '','', tleM.LoggedExercise)
//             dataPoint.LoggedExercise = tleM.LoggedExercise;
//             dataPoint.IsMetric = set.IsMetric;
//             dataPoints.push(dataPoint);
//         });
//         return dataPoints;
//     }

//     public toDateDataPoint = function(tleM: TransmutedLoggedExerciseMeraclusEntity, independentAxisValue: number): MeraclusDataPointEntity[]{   
//         // let dataPoints = new Array<ExerciseDataPoint>();
//         // tleM.LoggedExercise.Set.forEach((set,index) => {
//         //     let dataPoint = new ExerciseDataPoint(set,set.Date, tleM.Measure, tleM.Measure)
//         //     dataPoint.LoggedExercise = tleM.LoggedExercise;
//         //     dataPoint.IsMetric = set.IsMetric;
//         //     dataPoints.push(dataPoint);
//         // });
//         // return dataPoints;
//         return null;
//     }

    
// }