// import {TransmutedLoggedExerciseMeraclusEntity} from '../../../../../entity/transmuted-logged-exercise/meraclus/transmuted-logged-exercise-meraclus.entity';
// import {LoggedExerciseEntity} from '../../../../../entity/logged-exercise.entity';
// import {Injectable} from '@angular/core';
// import {SetService} from '../../../../generic/set-service/set.service';
// import {MEASURE_WEIGHT, MEASURE_HEIGHT, MEASURE_DISTANCE, MEASURE_TIME, MEASURE_REPS} from '../../../../../../global/service/global.service';

// export interface IMeraclusMeasure {
//     getWeight(loggedExercise: LoggedExerciseEntity): TransmutedLoggedExerciseMeraclusEntity;
//     getHeight(loggedExercise: LoggedExerciseEntity): TransmutedLoggedExerciseMeraclusEntity;
//     getDistance(loggedExercise: LoggedExerciseEntity): TransmutedLoggedExerciseMeraclusEntity;
//     getTime(loggedExercise: LoggedExerciseEntity): TransmutedLoggedExerciseMeraclusEntity;
//     getReps(loggedExercise: LoggedExerciseEntity): TransmutedLoggedExerciseMeraclusEntity;
// }

// @Injectable()
// export class MeraclusStatisticsService implements IMeraclusMeasure  {

//     constructor(private sS: SetService) { }

//     getWeight(loggedExercise: LoggedExerciseEntity): TransmutedLoggedExerciseMeraclusEntity {
//         let meraclus = new TransmutedLoggedExerciseMeraclusEntity(loggedExercise);
//         meraclus.Measure = MEASURE_WEIGHT;
//         return meraclus;
//     }
//     getHeight(loggedExercise: LoggedExerciseEntity): TransmutedLoggedExerciseMeraclusEntity {
//         let meraclus = new TransmutedLoggedExerciseMeraclusEntity(loggedExercise);
//         meraclus.Measure = MEASURE_HEIGHT;
//         return meraclus;
//     }
//     getDistance(loggedExercise: LoggedExerciseEntity): TransmutedLoggedExerciseMeraclusEntity {
//         let meraclus = new TransmutedLoggedExerciseMeraclusEntity(loggedExercise);
//         meraclus.Measure = MEASURE_DISTANCE;
//         return meraclus;
//     }
//     getTime(loggedExercise: LoggedExerciseEntity): TransmutedLoggedExerciseMeraclusEntity {
//         let meraclus = new TransmutedLoggedExerciseMeraclusEntity(loggedExercise);
//         meraclus.Measure = MEASURE_TIME;
//         return meraclus;
//     }
//     getReps(loggedExercise: LoggedExerciseEntity): TransmutedLoggedExerciseMeraclusEntity {
//         let meraclus = new TransmutedLoggedExerciseMeraclusEntity(loggedExercise);
//         meraclus.Measure = MEASURE_REPS;
//         return meraclus;
//     }
// }