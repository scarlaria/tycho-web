export interface IDataPointLoader {
    toDataPoint
    toDateDataPoint
}