import {Injectable} from '@angular/core';
import {SetService} from '../../../../../../../set/service/set/set.service';
import {TransmutedLoggedExerciseAverage} from '../../../../../../entity/transmuted-logged-exercise/average/transmuted-logged-exercise-average';
import {TransmutedLoggedExerciseMeraclusEntity} from '../../../../../../entity/transmuted-logged-exercise/meraclus/transmuted-logged-exercise-meraclus.entity';
import {TransmutedLoggedExercisePR} from '../../../../../../entity/transmuted-logged-exercise/pr/transmuted-logged-exercise-pr';

@Injectable()
export class IndependentXLoaderService {

constructor(private sS: SetService) { }

    // loadAverage(tleA: TransmutedLoggedExerciseAverage): IndependentXEntity {
    //     let dependentX = new IndependentXEntity(tleA.AverageValue, tleA.Measure);
    //     return dependentX;
    // }

    // loadPR(tlePR: TransmutedLoggedExercisePR): IndependentXEntity {
    //     let dependentX = new IndependentXEntity(tlePR.PRValue.max, tlePR.Measure + (tlePR.PRValue.index+1))
    //     return dependentX;
    // }

    // loadMeraclus(tleM: TransmutedLoggedExerciseMeraclusEntity): IndependentXEntity[]{
    //     let dependentXs = new Array<DependentYEntity>();
    //     let arr = this.sS.purgeSet(tleM.LoggedExercise, tleM.Measure);

    //     arr.forEach((set,index) => {
    //         let dependentX = new IndependentXEntity(set, tleM.Measure + ' | Set #: ' + index+1);
    //         dependentXs.push(dependentX);
    //     })
    //     return dependentXs;
    // }

}