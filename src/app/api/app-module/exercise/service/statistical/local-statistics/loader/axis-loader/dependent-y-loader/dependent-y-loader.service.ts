import {IAxesLoader} from '../IAxesLoader.interface';
import { Injectable } from '@angular/core';
import {SetService} from '../../../../../../../set/service/set/set.service';
import {TransmutedLoggedExerciseAverage} from '../../../../../../entity/transmuted-logged-exercise/average/transmuted-logged-exercise-average';
import {TransmutedLoggedExerciseMeraclusEntity} from '../../../../../../entity/transmuted-logged-exercise/meraclus/transmuted-logged-exercise-meraclus.entity';
import {TransmutedLoggedExercisePR} from '../../../../../../entity/transmuted-logged-exercise/pr/transmuted-logged-exercise-pr';

export interface IDependentYLoader extends IAxesLoader {
}

@Injectable()
export class DependentYLoaderService 
// implements IDependentYLoader 
{

    constructor(private sS: SetService) { }

    /**
     * Loads the Y-values for the potential chart
     * 
     * 
     * @memberOf DependentYLoaderService
     */
    // loadAverage(tleA: TransmutedLoggedExerciseAverage): DependentYEntity {
    //     let dependentY = new DependentYEntity(tleA.AverageValue, tleA.Measure);
    //     return dependentY;
    // }

    // loadPR(tlePR: TransmutedLoggedExercisePR): DependentYEntity {
    //     let dependentY = new DependentYEntity(tlePR.PRValue.max, tlePR.Measure + (tlePR.PRValue.index+1))
    //     return dependentY;
    // }

    // loadMeraclus(tleM: TransmutedLoggedExerciseMeraclusEntity): DependentYEntity[]{
    //     let dependentYs = new Array<DependentYEntity>();
    //     let arr = this.sS.purgeSet(tleM.LoggedExercise, tleM.Measure);

    //     arr.forEach((set,index) => {
    //         let dependentY = new DependentYEntity(set, tleM.Measure + ' | Set #: ' + index+1);
    //         dependentYs.push(dependentY);
    //     })
    //     return dependentYs;
    // }

}