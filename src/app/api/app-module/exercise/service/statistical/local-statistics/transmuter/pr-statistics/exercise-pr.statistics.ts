// import {LoggedExerciseEntity} from '../../../../../entity/logged-exercise.entity';
// import {TransmutedLoggedExercisePR} from '../../../../../entity/transmuted-logged-exercise/pr/transmuted-logged-exercise-pr';
// import {Injectable} from '@angular/core';
// import {MEASURE_WEIGHT, GlobalService, MEASURE_HEIGHT, MEASURE_DISTANCE, MEASURE_TIME, MEASURE_REPS} from '../../../../../../global/service/global.service';
// import {SetService} from '../../../../generic/set-service/set.service';


// export interface IExercisePRStatistics{
//     getBestWeight(loggedExercise: LoggedExerciseEntity): TransmutedLoggedExercisePR;
//     getBestHeight(loggedExercise: LoggedExerciseEntity): TransmutedLoggedExercisePR;
//     getBestReps(loggedExercise: LoggedExerciseEntity): TransmutedLoggedExercisePR;
//     getBestTime(loggedExercise: LoggedExerciseEntity): TransmutedLoggedExercisePR;
//     getBestDistance(loggedExercise: LoggedExerciseEntity): TransmutedLoggedExercisePR;
// }

// export interface IPRMeasure {
//     prWeight(loggedExercise: LoggedExerciseEntity): { max: number; index: number; };
//     prHeight(loggedExercise: LoggedExerciseEntity): { max: number; index: number; };
//     prDistance(loggedExercise: LoggedExerciseEntity):   { max: number; index: number; };
//     prTime(loggedExercise: LoggedExerciseEntity):   { max: number; index: number; };
//     prReps(loggedExercise: LoggedExerciseEntity):   { max: number; index: number; };
// }

// /**
//  * Responsible for all PR values retrieved per logged-exercise
//  * 
//  * @export
//  * @class ExercisePRStatisticsService
//  * @implements {IExercisePRStatistics}
//  * @implements {IPRMeasure}
//  */
// @Injectable()
// export class ExercisePRStatisticsService implements IExercisePRStatistics, IPRMeasure {

//     /**
//      * Returns the max for a particular logged-exercise based on weight
//      * 
//      * @param {LoggedExerciseEntity} loggedExercise 
//      * @returns {{ max: number; index: number; }} 
//      * 
//      * @memberOf ExercisePRStatisticsService
//      */
//     prWeight(loggedExercise: LoggedExerciseEntity):{ max: number; index: number; } {
//         let purgedSets = this.sS.purgeSet(loggedExercise, MEASURE_WEIGHT);
//         return GlobalService.getMax(purgedSets);
//     }

//     /**
//      * Returns the max for a particular logged-exercise based on height
//      * 
//      * @param {LoggedExerciseEntity} loggedExercise 
//      * @returns {{ max: number; index: number; }} 
//      * 
//      * @memberOf ExercisePRStatisticsService
//      */
//     prHeight(loggedExercise: LoggedExerciseEntity):{ max: number; index: number; } {
//         let purgedSets = this.sS.purgeSet(loggedExercise, MEASURE_HEIGHT);
//         return GlobalService.getMax(purgedSets);
//     }

//     /**
//      * Returns the max for a particular logged-exercise based on distance
//      * 
//      * @param {LoggedExerciseEntity} loggedExercise 
//      * @returns {{ max: number; index: number; }} 
//      * 
//      * @memberOf ExercisePRStatisticsService
//      */
//     prDistance(loggedExercise: LoggedExerciseEntity):{ max: number; index: number; } {
//         let purgedSets = this.sS.purgeSet(loggedExercise, MEASURE_DISTANCE);
//         return GlobalService.getMax(purgedSets);
//     }

//     /**
//      * Returns the max for a particular logged-exercise based on time
//      * 
//      * @param {LoggedExerciseEntity} loggedExercise 
//      * @returns {{ max: number; index: number; }} 
//      * 
//      * @memberOf ExercisePRStatisticsService
//      */
//     prTime(loggedExercise: LoggedExerciseEntity):{ max: number; index: number; } {
//         let purgedSets = this.sS.purgeSet(loggedExercise, MEASURE_TIME);
//         return GlobalService.getMax(purgedSets);
//     }

//     /**
//      * Returns the max for a particular logged-exercise based on reps
//      * 
//      * @param {LoggedExerciseEntity} loggedExercise 
//      * @returns {{ max: number; index: number; }} 
//      * 
//      * @memberOf ExercisePRStatisticsService
//      */
//     prReps(loggedExercise: LoggedExerciseEntity):{ max: number; index: number; } {
//         let purgedSets = this.sS.purgeSet(loggedExercise, MEASURE_REPS);
//         return GlobalService.getMax(purgedSets);
//     }
    
//     constructor(private sS: SetService) { }

//     /**
//      * Returns the PR for a particular logged-exercise based on weight
//      * 
//      * @param {LoggedExerciseEntity} loggedExercise 
//      * @returns {TransmutedLoggedExercisePR} 
//      * 
//      * @memberOf ExercisePRStatisticsService
//      */
//     public getBestWeight(loggedExercise: LoggedExerciseEntity): TransmutedLoggedExercisePR {
//         let pr = this.prWeight(loggedExercise);
//         let transmutedPR = new TransmutedLoggedExercisePR(loggedExercise);
//         transmutedPR.PRValue = pr;
//         transmutedPR.Measure = MEASURE_WEIGHT;
//         return transmutedPR;
//     }

//     /**
//      * Returns the PR for a particular logged-exercise based on height
//      * 
//      * @param {LoggedExerciseEntity} loggedExercise 
//      * @returns {TransmutedLoggedExercisePR} 
//      * 
//      * @memberOf ExercisePRStatisticsService
//      */
//     public getBestHeight(loggedExercise: LoggedExerciseEntity): TransmutedLoggedExercisePR {
//         let pr = this.prHeight(loggedExercise);
//         let transmutedPR = new TransmutedLoggedExercisePR(loggedExercise);
//         transmutedPR.PRValue = pr;
//         transmutedPR.Measure = MEASURE_HEIGHT;
//         return transmutedPR;
//     }

//     /**
//      * Returns the PR for a particular logged-exercise based on Reps
//      * 
//      * @param {LoggedExerciseEntity} loggedExercise 
//      * @returns {TransmutedLoggedExercisePR} 
//      * 
//      * @memberOf ExercisePRStatisticsService
//      */
//     public getBestReps(loggedExercise: LoggedExerciseEntity): TransmutedLoggedExercisePR {
//         let pr = this.prReps(loggedExercise);
//         let transmutedPR = new TransmutedLoggedExercisePR(loggedExercise);
//         transmutedPR.PRValue = pr;
//         transmutedPR.Measure = MEASURE_REPS;
//         return transmutedPR;
//     }

//     /**
//      * Returns the PR for a particular logged-exercise based on Distance
//      * 
//      * @param {LoggedExerciseEntity} loggedExercise 
//      * @returns {TransmutedLoggedExercisePR} 
//      * 
//      * @memberOf ExercisePRStatisticsService
//      */
//     public getBestDistance(loggedExercise: LoggedExerciseEntity): TransmutedLoggedExercisePR {
//         let pr = this.prDistance(loggedExercise);
//         let transmutedPR = new TransmutedLoggedExercisePR(loggedExercise);
//         transmutedPR.PRValue = pr;
//         transmutedPR.Measure = MEASURE_DISTANCE;
//         return transmutedPR;
//     }

//     /**
//      * Returns the PR for a particular logged-exercise based on Time
//      * 
//      * @param {LoggedExerciseEntity} loggedExercise 
//      * @returns {TransmutedLoggedExercisePR} 
//      * 
//      * @memberOf ExercisePRStatisticsService
//      */
//     public getBestTime(loggedExercise: LoggedExerciseEntity): TransmutedLoggedExercisePR {
//         let pr = this.prTime(loggedExercise);
//         let transmutedPR = new TransmutedLoggedExercisePR(loggedExercise);
//         transmutedPR.PRValue = pr;
//         transmutedPR.Measure = MEASURE_TIME;
//         return transmutedPR;
//     }
// }

// //octkp qodwtc