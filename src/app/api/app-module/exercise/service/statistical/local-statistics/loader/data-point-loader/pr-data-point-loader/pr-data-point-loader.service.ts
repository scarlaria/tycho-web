// import { DependentYEntity } from '../../../../../../../chart/entity/axes/dependent-y/dependent-y.entity';
// import { IndependentXEntity } from '../../../../../../../chart/entity/axes/independent-x/independent-x.entity';
// import { PRDataPointEntity } from '../../../../../../entity/data-point/pr-data-point/pr-data-point.entity';
// import {ExerciseDataPoint} from '../../../../../../entity/exercise-data-point';
// import {
//     TransmutedLoggedExercisePR
// } from '../../../../../../entity/transmuted-logged-exercise/pr/transmuted-logged-exercise-pr';
// import { IDataPointLoader } from '../idata-point-loader.interface';
// import {Injectable} from '@angular/core';

// export interface IPRDataPointLoader extends IDataPointLoader {

// }

// @Injectable()
// export class PRDataPointLoaderService implements IPRDataPointLoader{
//     /**
//      * Returns a data-point representing a single point an a graph
//      * 
//      * 
//      * @memberOf MeraclusDataPointLoaderService
//      */
//     public toDataPoint = function(dependentY: TransmutedLoggedExercisePR, independentX: TransmutedLoggedExercisePR): PRDataPointEntity{   
//         let x = new IndependentXEntity(independentX.PRValue.max, independentX.Measure);
//         let y = new DependentYEntity(dependentY.PRValue.max, dependentY.Measure);
//         let dataPoint = new ExerciseDataPoint(y,x, y.Description, x.Description, dependentY.LoggedExercise);
//         dataPoint.LoggedExercise = dependentY.LoggedExercise;
//         return dataPoint;
//     }

//     public toDateDataPoint = function(dependentY: TransmutedLoggedExercisePR, independentX: TransmutedLoggedExercisePR): PRDataPointEntity{   
//         // let dataPoint = new ExerciseDataPoint(dependentY.PRValue,independentX.LoggedExercise.ExerciseDate,dependentY.Measure, independentX.Measure);
//         // dataPoint.LoggedExercise = dependentY.LoggedExercise;
//         // return dataPoint;
//         return null;
//     }
// }