export interface IAxesLoader {
    loadAverage
    loadPR
    loadMeraclus
}