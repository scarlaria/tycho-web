export interface IChartLoader {
    toLineChart
    toBarChart
    toRadarChart
    toBubbleChart
    toPieChart
    toPolarAreaChart
    toDoughnutChart
}