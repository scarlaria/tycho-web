import * as firebase from 'firebase/app';

import { AsyncSubject, Observable, Observer, Subject } from 'rxjs/Rx';

import {AngularFireAuth} from 'angularfire2/auth';
import {AngularFireDatabase} from 'angularfire2/database';
import {Injectable} from '@angular/core';

@Injectable()
export class ExerciseStatisticsService {

    constructor(private afDB: AngularFireDatabase, private afAuth: AngularFireAuth) {}

//     compareAverageWeight(exercise1: QuerydLoggedExerciseEntity, 
//             exercise2: QuerydLoggedExerciseEntity): Array<ExerciseTimeline> {
//         let exerciseTimeLines = new Array<ExerciseTimeline>();

//         // let exercise1TimeLine = this.averageWeightTimeline(exercise1);
//         // exerciseTimeLines.push(exercise1TimeLine);
//         // let exercise2TimeLine = this.averageWeightTimeline(exercise2);
//         // exerciseTimeLines.push(exercise2TimeLine);

//         return exerciseTimeLines
//     }

//     /**
//      * Retrieves the rank of the exercise in a global context
//      * 
//      * @param {string} exerciseId 
//      * @returns {Observable<number>} 
//      * 
//      * @memberOf ExerciseStatisticsService
//      */
//     globalCompletionRank(exerciseId: string): Observable<number> {
//         let observable = new Observable<number>();
//         let value = 0;
//         this.afAuth.authState.subscribe(user=> {
//             // if(user) {
//             //     this.afDB.list(DB_EXERCISE + DB_DASH + exerciseId, {
//             //     })
//             //         .subscribe(globalCompletion => {
//             //             value = globalCompletion
//             //             return Observable.of(globalCompletion);
//             //         })
//             // }
//         })
//         return Observable.of(value);
//     }

//     /**
//      * Retrieves the local completion rank for a particular exercise
//      * 
//      * @param {string} exerciseId 
//      * @returns {number} 
//      * 
//      * @memberOf ExerciseStatisticsService
//      */
//     localCompletionRank(exerciseId: string): number {
//         return null;
//     }

//     /**
//      * Retrieves the global completion for a particular exercise
//      * 
//      * @param {string} exerciseId 
//      * @returns {Observable<number>}    
//      * 
//      * @memberOf ExerciseStatisticsService
//      */
//     globalCompletion(exerciseId: string): Observable<number> {
//         let observable = new Observable<number>();
//         let value = 0;
//         this.afAuth.authState.subscribe(user=> {
//             if(user) {
//                 this.afDB.object(DB_EXERCISE + DB_DASH + exerciseId + DB_EXERCISE_GLOBAL_COMPLETION)
//                 .subscribe(globalCompletion => {
//                     value = globalCompletion
//                     return Observable.of(globalCompletion);
//                 })
//             }
//         })
//         return Observable.of(value);
//     }

//     /**
//      * Increments the global completion for a particular exercise
//      * 
//      * @param {string} exerciseId 
//      * @returns 
//      * 
//      * @memberOf ExerciseStatisticsService
//      */
//     incrementGlobalCompletion(exerciseId: string) {
//         let observable = new Observable();
//         this.afAuth.authState.subscribe(user=> {
//             if(user) {
//                 this.afDB.object(DB_EXERCISE + DB_DASH + exerciseId + DB_EXERCISE_GLOBAL_COMPLETION)
//                 .$ref.transaction(x => {
//                     console.log('Transacting')
//                     x++;
//                 })
//             }
//         })
//         return Observable.of(0);
//     }

//     decrementGlobalCompletion() {

//     }

//     /**
//      * Retrieves the number of times a user has completed a workout
//      * 
//      * @param {string} exerciseId 
//      * @returns {Observable<number>} 
//      * 
//      * @memberOf ExerciseStatisticsService
//      */
//     localCompletion(exerciseId: string): Observable<number> {
//         let observable = new Observable();
//         this.afAuth.authState.subscribe(user=> {
//             if(user) {
//                 this.afDB.list(DB_LOGGED_EXERCISE_BY_USER + '/' + user.uid, {
//                     query: {
//                         orderByChild: exerciseId,
//                         equalTo:exerciseId
//                     }
//                 }).subscribe(loggedExerciseIds => {
//                     return Observable.of(loggedExerciseIds.length);
//                 })
//             }
//         })
//         return Observable.of(0);
//     }

 
//     /**
//      * Retrieves the weight breakdown for a logged exercise. When a user completes an exercise that contains 
//      * 1 or many sets, the breakdown shows each set of the day.
//      * @param {LoggedExerciseEntity} loggedExercise 
//      * @returns {ExerciseTimeline} 
//      * 
//      * @memberOf ExerciseStatisticsService
//      */
//     weightBreakdownPerDay(loggedExercise$: Observable<LoggedExerciseEntity>): Observable<ExerciseTimeline> {
        
//         let exerciseId = '';

//       return null;
//     }

//     /**
//      * Retrieves the height breakdown for a logged exercise. When a user completes an exercise that contains 
//      * 1 or many sets, the breakdown shows each set of the day.
//      * @param {LoggedExerciseEntity} loggedExercise 
//      * @returns {ExerciseTimeline} 
//      * 
//      * @memberOf ExerciseStatisticsService
//      */
//     timeBreakdownPerDay(loggedExercise: LoggedExerciseEntity): ExerciseTimeline {
//         // let dataPoints = new Array<ExerciseDataPoint>();
       
//         // loggedExercise.Set.forEach((set, index) => {
//         //     let dataPoint = new ExerciseDataPoint(set.Date, set.Time, 'Time');
//         //     dataPoints.push(dataPoint);
//         // })
//         // let timeLine = new ExerciseTimeline(dataPoints, 'Time Breakdown Per Day', loggedExercise.ExerciseId, 'Set');

//         // return timeLine;     
//         return null;  
//     }    

//     /**
//      * Retrieves the distance breakdown for a logged exercise. When a user completes an exercise that contains 
//      * 1 or many sets, the breakdown shows each set of the day.
//      * 
//      * @param {LoggedExerciseEntity} loggedExercise 
//      * @returns {ExerciseTimeline} 
//      * 
//      * @memberOf ExerciseStatisticsService
//      */
//     distanceBreakdownPerDay(loggedExercise: LoggedExerciseEntity): ExerciseTimeline {
//         // let dataPoints = new Array<ExerciseDataPoint>();
       
//         // loggedExercise.Set.forEach((set, index) => {
//         //     let dataPoint = new ExerciseDataPoint(set.Date, set.Distance, 'Distance');
//         //     dataPoints.push(dataPoint);
//         // })
//         // let timeLine = new ExerciseTimeline(dataPoints, 'Distance Breakdown Per Day', loggedExercise.ExerciseId, 'Set');

//         // return timeLine;       

//                 return null;  
//     }
   
//    /**
//     * Retrieves the reps breakdown for a logged exercise. When a user completes an exercise that contains 
//      * 1 or many sets, the breakdown shows each set of the day.
//     * @param {LoggedExerciseEntity} loggedExercise 
//     * @returns {ExerciseTimeline} 
//     * 
//     * @memberOf ExerciseStatisticsService
//     */
//     repsBreakdownPerDay(loggedExercise: LoggedExerciseEntity): ExerciseTimeline {
//         // let dataPoints = new Array<ExerciseDataPoint>();
       
//         // loggedExercise.Set.forEach((set, index) => {
//         //     let dataPoint = new ExerciseDataPoint(set.Date, set.Reps, 'Reps');
//         //     dataPoints.push(dataPoint);
//         // })
//         // let timeLine = new ExerciseTimeline(dataPoints, 'Rep Breakdown Per Day', loggedExercise.ExerciseId, 'Set');

//                 return null;        
//     }
    
//     /**
//      * Retrieves the height breakdown for a logged exercise. When a user completes an exercise that contains 
//      * 1 or many sets, the breakdown shows each set of the day. 
//      * @param {LoggedExerciseEntity} loggedExercise 
//      * @returns {ExerciseTimeline} 
//      * 
//      * @memberOf ExerciseStatisticsService
//      */
//     heightBreakdownPerDay(loggedExercise: LoggedExerciseEntity): ExerciseTimeline {
//         // let dataPoints = new Array<ExerciseDataPoint>();
       
//         // loggedExercise.Set.forEach((set, index) => {
//         //     let dataPoint = new ExerciseDataPoint(set.Date, set.Height, 'Height');
//         //     dataPoints.push(dataPoint);
//         // })

//         // let timeLine = new ExerciseTimeline(dataPoints, 'Height Breakdown Per Day', loggedExercise.ExerciseId, 'Set');
//         // return timeLine;       
//             return null;  
//     }       
//     /**
//      * Returns a list of datapoints for the average of a workout.
//      * 
//      * @param {QuerydLoggedExerciseEntity} exercise 
//      * @returns {Array<ExerciseDataPoint>} 
//      * 
//      * @memberOf ExerciseStatisticsService
//      */
//     averageWeightTimeline(querydExercise: QuerydLoggedExerciseEntity): ExerciseTimeline {
//         // let dataPoints = new Array<ExerciseDataPoint>();
//         // let exerciseTimeLine;
//         //         querydExercise.LoggedExercises.forEach(loggedExercise => {
//         //             console.log(loggedExercise);
//         //             let weight = 0;
//         //             let date = 0;
//         //             let counter = 0;
//         //             loggedExercise.Set.forEach((set,index) => {
//         //                 if(set.Weight != -1) {
//         //                     weight = weight + set.Weight;
//         //                     date = set.Date;
//         //                     counter = index;
//         //                 }
//         //             })
//         //             let average = (weight/counter);
//         //             let dataPoint = new ExerciseDataPoint(date, average, 'Weight');
//         //             console.log('average: ' + average);
//         //             dataPoints.push(dataPoint);
//         //         })
//         //      exerciseTimeLine = new ExerciseTimeline(dataPoints, 'Average Weight', 'querydExercise.ExerciseId', 'Weight');
//         // return exerciseTimeLine;
//                 return null;  
//     }

//     /**
//      * Returns a list of datapoints for the average of a workout.
//      * 
//      * @param {QuerydLoggedExerciseEntity} exercise 
//      * @returns {Array<ExerciseDataPoint>} 
//      * 
//      * @memberOf ExerciseStatisticsService
//      */
//     averageHeightTimeline(exercise: QuerydLoggedExerciseEntity): ExerciseTimeline {
//         // let dataPoints = new Array<ExerciseDataPoint>();
//         // exercise.LoggedExercises.forEach(loggedExercise => {
//         //     let height = 0;
//         //     let date = 0;
//         //     let counter = 0;
//         //     loggedExercise.Set.forEach((set,index) => {
//         //         if(set.Height != -1) {
//         //             height = height + set.Height;
//         //             date = set.Date;
//         //             counter = index;
//         //         }
//         //     })
//         //     let average = (height/counter);
//         //     let dataPoint = new ExerciseDataPoint(date, average, 'Height');
//         //     dataPoints.push(dataPoint);
//         // })
//         // let exerciseTimeLine = new ExerciseTimeline(dataPoints, 'Average Height', exercise.ExerciseId, 'Height');
//         // return exerciseTimeLine;
//                 return null;  
//     }

//     /**
//      * Returns a list of datapoints for the average of a workout.
//      * 
//      * @param {QuerydLoggedExerciseEntity} exercise 
//      * @returns {Array<ExerciseDataPoint>} 
//      * 
//      * @memberOf ExerciseStatisticsService
//      */
//     averageDistanceTimeline(exercise: QuerydLoggedExerciseEntity): ExerciseTimeline {
//         // let dataPoints = new Array<ExerciseDataPoint>();
//         // exercise.LoggedExercises.forEach(loggedExercise => {
//         //     let distance = 0;
//         //     let date = 0;
//         //     let counter = 0;
//         //     loggedExercise.Set.forEach((set,index) => {
//         //         if(set.Distance != -1) {
//         //             distance = distance + set.Distance;
//         //             date = set.Date;
//         //             counter = index;
//         //         }
//         //     })
//         //     let average = (distance/counter);
//         //     let dataPoint = new ExerciseDataPoint(date, average, 'Distance');
//         //     dataPoints.push(dataPoint);
//         // })

//         // let exerciseTimeLine = new ExerciseTimeline(dataPoints, 'Average Distance', exercise.ExerciseId, 'Distance');
//         // return exerciseTimeLine;
//                 return null;  
//     }

//     /**
//      * Returns a list of datapoints for the average of a workout.
//      * 
//      * @param {QuerydLoggedExerciseEntity} exercise 
//      * @returns {Array<ExerciseDataPoint>} 
//      * 
//      * @memberOf ExerciseStatisticsService
//      */
//     averageRepsTimeline(exercise: QuerydLoggedExerciseEntity): ExerciseTimeline {
//         // let dataPoints = new Array<ExerciseDataPoint>();
//         // exercise.LoggedExercises.forEach(loggedExercise => {
//         //     let reps = 0;
//         //     let date = 0;
//         //     let counter = 0;
//         //     loggedExercise.Set.forEach((set,index) => {
//         //         if(set.Reps != -1) {
//         //             reps = reps + set.Reps;
//         //             date = set.Date;
//         //             counter = index;
//         //         }
//         //     })
//         //     let average = (reps/counter);
//         //     let dataPoint = new ExerciseDataPoint(date, average, 'Reps');
//         //     dataPoints.push(dataPoint);
//         // })

//         // let exerciseTimeLine = new ExerciseTimeline(dataPoints, 'Average Reps', exercise.ExerciseId, 'Reps');
//         // return exerciseTimeLine;
//                 return null;  
//     }

//     /**
//      * Returns a list of datapoints for the average of a workout.
//      * 
//      * @param {QuerydLoggedExerciseEntity} exercise 
//      * @returns {Array<ExerciseDataPoint>} 
//      * 
//      * @memberOf ExerciseStatisticsService
//      */
//     averageTimeTimeline(exercise: QuerydLoggedExerciseEntity): ExerciseTimeline {
//         // let dataPoints = new Array<ExerciseDataPoint>();
//         // exercise.LoggedExercises.forEach(loggedExercise => {
//         //     let time = 0;
//         //     let date = 0;
//         //     let counter = 0;
//         //     loggedExercise.Set.forEach((set,index) => {
//         //         if(set.Time != -1) {
//         //             time = time + set.Time;
//         //             date = set.Date;
//         //             counter = index;
//         //         }
//         //     })
//         //     let average = (time/counter);
//         //     let dataPoint = new ExerciseDataPoint(date, average, 'time');
//         //     dataPoints.push(dataPoint);
//         // })

//         // let exerciseTimeLine = new ExerciseTimeline(dataPoints, 'Average Time', exercise.ExerciseId, 'Time');
//         // return exerciseTimeLine;
//                 return null;  
//     }   

//     /**
//      * Returns a list of datapoints for the average of a workout.
//      * ###NF
//      * @param {QuerydLoggedExerciseEntity} exercise 
//      * @returns {Array<ExerciseDataPoint>} 
//      * 
//      * @memberOf ExerciseStatisticsService
//      */
//     averageSetsForExerciseTimeline(querydExercise$: Observable<QuerydLoggedExerciseEntity>): Observable<ExerciseTimeline> {
//         /**1. Get the number of sets for each instance of the exercise*/
//         querydExercise$.subscribe(x=> {
//             x.LoggedExercises.forEach(loggedExercise => {
//                 loggedExercise.Set
//             })
//         })

//         return null;

//     }

//     retrieveGoals(): Observable<any> {
//         return null;
//     }

    

 
}

