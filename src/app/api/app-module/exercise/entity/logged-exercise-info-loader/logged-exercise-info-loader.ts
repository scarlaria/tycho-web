import {MEASURE_DISTANCE, MEASURE_HEIGHT, MEASURE_REPS, MEASURE_TIME, MEASURE_WEIGHT} from '../../../../../globals/const/global.const';

import { Injectable } from '@angular/core';
import {LoggedExerciseEntity} from '../logged-exercise/logged-exercise.entity';
import { Observable } from 'rxjs/Rx';
import {SetService} from '../../../set/service/set/set.service';

/**
 * The format for the methods is {y}by{x}...
 *
 * @export
 * @interface ILoggedExerciseMeasureSupplanter
 */
export interface ILoggedExerciseMeasureSupplanter {

    /**
     * MEASURES
     * Reps, Weight, Time, Distance, Height
     *
     * @memberOf ILoggedExerciseMeasureSupplanter
     */

    /**
     * Returns an ExerciseDataPoint with Reps on Y and Weight on X
     * 
     * @param {Observable < LoggedExerciseEntity >} loggedExercise$ 
     * @returns {Observable < ExerciseDataPoint[] >} 
     * 
     * @memberOf ILoggedExerciseMeasureSupplanter
     */
//     repsByWeight(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] >;
//     weightByReps(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] >;

//     repsByDistance(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] >;
//     distanceByReps(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] >;

//     repsByHeight(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] >;
//     heightByReps(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] >;

//     repsByTime(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] >;
//     timeByReps(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] >;

//     weightByTime(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] >;
//     timeByWeight(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[]>;

//     weightByDistance(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] >;
//     distanceByWeight(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] >;

//     weightByHeight(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] >;
//     heightByWeight(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] >;

//     timeByDistance(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] >;
//     distanceByTime(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] >;

//     timeByHeight(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] >;
//     heightByTime(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] >;

//     distanceByHeight(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] >;
//     heightByDistance(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] >;
// }

// @Injectable()
// export class LoggedExerciseInfoLoader implements ILoggedExerciseMeasureSupplanter {

//     repsByWeight(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] > {
//         return loggedExercise$.map(loggedExercise => SetService.retrieveDataPoints(loggedExercise, MEASURE_REPS, MEASURE_WEIGHT))
//     }
//     weightByReps(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] > {
//         return loggedExercise$.map(loggedExercise => SetService.retrieveDataPoints(loggedExercise, MEASURE_WEIGHT, MEASURE_REPS))
//     }

//     repsByDistance(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] > {
//         return loggedExercise$.map(loggedExercise => SetService.retrieveDataPoints(loggedExercise, MEASURE_REPS, MEASURE_DISTANCE))
//     }
//     distanceByReps(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] > {
//         return loggedExercise$.map(loggedExercise => SetService.retrieveDataPoints(loggedExercise, MEASURE_DISTANCE, MEASURE_REPS))
//     }

//     repsByHeight(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] > {
//         return loggedExercise$.map(loggedExercise => SetService.retrieveDataPoints(loggedExercise, MEASURE_REPS, MEASURE_HEIGHT))
//     }
//     heightByReps(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] > {
//         return loggedExercise$.map(loggedExercise => SetService.retrieveDataPoints(loggedExercise, MEASURE_HEIGHT, MEASURE_REPS))
//     }

//     repsByTime(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] > {
//         return loggedExercise$.map(loggedExercise => SetService.retrieveDataPoints(loggedExercise, MEASURE_REPS, MEASURE_TIME))
//     }
//     timeByReps(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] > {
//         return loggedExercise$.map(loggedExercise => SetService.retrieveDataPoints(loggedExercise, MEASURE_TIME, MEASURE_REPS))
//     }

//     weightByTime(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] > {
//         return loggedExercise$.map(loggedExercise => SetService.retrieveDataPoints(loggedExercise, MEASURE_WEIGHT, MEASURE_TIME))
//     }
//     timeByWeight(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] > {
//         return loggedExercise$.map(loggedExercise => SetService.retrieveDataPoints(loggedExercise, MEASURE_TIME, MEASURE_WEIGHT))
//     }

//     weightByDistance(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] > {
//         return loggedExercise$.map(loggedExercise => SetService.retrieveDataPoints(loggedExercise, MEASURE_WEIGHT, MEASURE_DISTANCE))
//     }
//     distanceByWeight(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] > {
//         return loggedExercise$.map(loggedExercise => SetService.retrieveDataPoints(loggedExercise, MEASURE_DISTANCE, MEASURE_WEIGHT))
//     }

//     weightByHeight(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] > {
//         return loggedExercise$.map(loggedExercise => SetService.retrieveDataPoints(loggedExercise, MEASURE_WEIGHT, MEASURE_HEIGHT))
//     }
//     heightByWeight(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] > {
//         return loggedExercise$.map(loggedExercise => SetService.retrieveDataPoints(loggedExercise, MEASURE_HEIGHT, MEASURE_WEIGHT))
//     }

//     timeByDistance(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] > {
//         return loggedExercise$.map(loggedExercise => SetService.retrieveDataPoints(loggedExercise, MEASURE_TIME, MEASURE_DISTANCE))
//     }
//     distanceByTime(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] > {
//         return loggedExercise$.map(loggedExercise => SetService.retrieveDataPoints(loggedExercise, MEASURE_DISTANCE, MEASURE_TIME))
//     }

//     timeByHeight(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] > {
//         return loggedExercise$.map(loggedExercise => SetService.retrieveDataPoints(loggedExercise, MEASURE_TIME, MEASURE_HEIGHT))
//     }
//     heightByTime(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] > {
//         return loggedExercise$.map(loggedExercise => SetService.retrieveDataPoints(loggedExercise, MEASURE_HEIGHT, MEASURE_TIME))
//     }

//     distanceByHeight(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] > {
//         return loggedExercise$.map(loggedExercise => SetService.retrieveDataPoints(loggedExercise, MEASURE_DISTANCE, MEASURE_HEIGHT))
//     }
//     heightByDistance(loggedExercise$ : Observable < LoggedExerciseEntity >) : Observable < ExerciseDataPoint[] > {
//         return loggedExercise$.map(loggedExercise => SetService.retrieveDataPoints(loggedExercise, MEASURE_HEIGHT, MEASURE_DISTANCE))
//     }

}