
export class MeasureEntity {
    private id : string;
    private name : string;
    private description : string;
  
    public constructor() {
    
    }

    public get Id() { return this.id;}
    public set Id(id : string) {this.id = id;}
    public set Name(name: string){this.name = name;}
    public get Name(){return this.name}
    public set Description (description) { this.description = description; } 
    public get Description () { return this.description; } 

    public static hyperInit(measure : MeasureEntity ) {
      if(!measure.Id) { measure.Id = ''; }
      if(!measure.Name) { measure.Name = ''; }
      if(!measure.Description) { measure.Description = ''; }
      return measure;
    }
  
    public static convertObjectToExerciseMeasure(key: string, obj : {}) : MeasureEntity {
      let measure = MeasureEntity.hyperInit(new MeasureEntity());
      measure.Id = key
      if(obj['name']) { measure.Name = obj['name']; }
      if(obj['description']) { measure.Description = obj['description']; }
      return measure;
    }

    public static convertObject(key: string, obj : {}) : MeasureEntity {
      let measure = MeasureEntity.hyperInit(new MeasureEntity());
      measure.Id = key
      if(obj['name']) { measure.Name = obj['name']; }
      if(obj['description']) { measure.Description = obj['description']; }
      return measure;
    }
}
