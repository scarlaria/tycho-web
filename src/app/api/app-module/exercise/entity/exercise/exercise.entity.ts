/**
 * Created by MartinO on 20/01/2017.
 */
import { Component } from '@angular/core';

@Component({

})

export class ExerciseEntity {
  
  private id : string;
  private category : string[];
  private image : string;
  private name : string;
  private owner : string;
  private createdDate : any;
  private updatedDate : any;
  private measure : string[];
  private description : string;
  private globalCompletion : number;
  private equipment : string[];
  private anatomy : string[];
  private isPrivate : boolean;
  private rating: number;
  private isArchived : boolean;

  private measureObject : {};
  private categoryObject : {};
  private equipmentObject : {};
  private anatomyObject : {};
  
  public constructor() {
    
  }
  
  public static convertObject(key: string, obj: {}) : ExerciseEntity {
    let ex = ExerciseEntity.hyperInit(new ExerciseEntity());
    ex.Id = key;
    
    if(obj['anatomy']) {  ex.AnatomyObject = obj['anatomy']; let keys = Object.keys(obj['anatomy']); keys.forEach(key => { ex.Anatomy.push(key)}); }
    if(obj['description']) { ex.Description = obj['description']; }
    if(obj['equipment']) { ex.EquipmentObject = obj['equipment']; let keys = Object.keys(obj['equipment']); keys.forEach(key => { ex.Equipment.push(key)});  }
    if(obj['globalCompletion']) { ex.GlobalCompletion = obj['globalCompletion']; }
    if(obj['image']) { ex.Image = obj['image']; }
    if(obj['isPrivate']) { ex.IsPrivate = obj['isPrivate']; }
    if(obj['measure']) { ex.MeasureObject = obj['measure']; let keys = Object.keys(obj['measure']); keys.forEach(key => { ex.Measure.push(key)});  }
    if(obj['name']) { ex.Name = obj['name']; }
    if(obj['owner']) { ex.Owner = obj['owner']; }
    if(obj['rating']) { ex.Rating = obj['rating']; }
    if(obj['updatedDate']) { ex.UpdatedDate = obj['updatedDate']; }
    if(obj['category']) {ex.CategoryObject = obj['category']; let keys = Object.keys(obj['category']); keys.forEach(key => { ex.Category.push(key)}); }  
    if(obj['isArchived']) { ex.IsArchived = obj['isArchived']; }
    return ex;
  }

  public static hyperInit(exercise : ExerciseEntity): ExerciseEntity {
    if(!exercise.Id)            { exercise.Id = ''; }
    if(!exercise.Anatomy)  { exercise.Anatomy = new Array(); }
    if(!exercise.Category) {exercise.Category = new Array(); }
    if(!exercise.Description) { exercise.Description = ''; }
    if(!exercise.Equipment) { exercise.Equipment = new Array(); }
    if(!exercise.GlobalCompletion) { exercise.GlobalCompletion = 0; }
    if(!exercise.Image) { exercise.Image = ''; }
    if(!exercise.IsPrivate) { exercise.IsPrivate = false; }
    if(!exercise.Measure) { exercise.Measure = new Array(); }
    if(!exercise.Name) { exercise.Name = ''; }
    if(!exercise.Owner) { exercise.Owner = ''; }
    if(!exercise.UpdatedDate) { exercise.UpdatedDate = ''; }
    if(!exercise.Rating) { exercise.Rating = 0; }
    if(!exercise.IsArchived) {exercise.IsArchived = false; }

    if(!exercise.CategoryObject) {exercise.CategoryObject = {}; }
    if(!exercise.MeasureObject) {exercise.MeasureObject = {}; }
    if(!exercise.EquipmentObject) {exercise.EquipmentObject = {}; }
    if(!exercise.AnatomyObject) {exercise.AnatomyObject = {}; }

    return exercise;
  }

public get Id() { return this.id;}
public set Id(id : string) {this.id = id;}
public set Name(name: string){this.name = name;}
public get Name(){return this.name}
public set Category (category) { this.category = category; } 
public get Category () { return this.category; } 
public set Image (image) { this.image = image; } 
public get Image () { return this.image; } 
public set Owner (owner) { this.owner = owner; } 
public get Owner () { return this.owner; } 
public set CreatedDate (createdDate) { this.createdDate = createdDate; } 
public get CreatedDate () { return this.createdDate; } 
public set UpdatedDate (updatedDate) { this.updatedDate = updatedDate; } 
public get UpdatedDate () { return this.updatedDate; } 
public set Measure (measure) { this.measure = measure; } 
public get Measure () { return this.measure; } 
public set Description (description) { this.description = description; } 
public get Description () { return this.description; } 
public set GlobalCompletion (globalCompletion) { this.globalCompletion = globalCompletion; } 
public get GlobalCompletion () { return this.globalCompletion; } 
public set Equipment (equipment) { this.equipment = equipment; } 
public get Equipment () { return this.equipment; } 
public set Anatomy (anatomy) { this.anatomy = anatomy; } 
public get Anatomy () { return this.anatomy; } 
public set IsPrivate (value){this.isPrivate = value;}
public get IsPrivate (){return this.isPrivate;}
public set IsArchived (value){this.isArchived = value;}
public get IsArchived (){return this.isArchived;}
public get Rating () {return this.rating}
public set Rating (rating : number) { this.rating = rating;}
	public get AnatomyObject(): {} {
		return this.anatomyObject;
	}

	public set AnatomyObject(value: {}) {
		this.anatomyObject = value;
	}

	public get EquipmentObject(): {} {
		return this.equipmentObject;
	}

	public set EquipmentObject(value: {}) {
		this.equipmentObject = value;
	}

	public get CategoryObject(): {} {
		return this.categoryObject;
	}

	public set CategoryObject(value: {}) {
		this.categoryObject = value;
	}
  
	public get MeasureObject(): {} {
		return this.measureObject;
	}

	public set MeasureObject(value: {}) {
		this.measureObject = value;
	}
  



}
