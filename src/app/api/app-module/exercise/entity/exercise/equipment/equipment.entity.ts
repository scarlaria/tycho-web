
export class EquipmentEntity {
  private id : string;
  private name : string;
  private description : string;
  
  public constructor() {
    
  }

  public get Id() { return this.id;}
  public set Id(id : string) {this.id = id;}
  public set Name(name: string){this.name = name;}
  public get Name(){return this.name}
  public set Description (description) { this.description = description; } 
  public get Description () { return this.description; } 

  public static hyperInit(equipment : EquipmentEntity ) {
    if(!equipment.Id) { equipment.Id = ''; }
    if(!equipment.Name) { equipment.Name = ''; }
    if(!equipment.Description) { equipment.Description = ''; }
    return equipment;
  }

  public static convertObjectToExerciseEquipment(key: string, obj : {}) : EquipmentEntity {
    let equipment = EquipmentEntity.hyperInit(new EquipmentEntity());
    equipment.Id = key;
    if(obj['name']) { equipment.Name = obj['name']; }
    if(obj['description']) { equipment.Description = obj['description']; }
    return equipment;
  }

}
