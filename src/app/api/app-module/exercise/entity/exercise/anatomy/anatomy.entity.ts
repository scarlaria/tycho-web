export class ExerciseAnatomyEntity {
  private id : string;
  private name : string;
  private description : string;
  
  public constructor() {
    
  }
    public get Id() { return this.id;}
    public set Id(id : string) {this.id = id;}
    public set Name(name: string){this.name = name;}
    public get Name(){return this.name}
    public set Description (description) { this.description = description; } 
    public get Description () { return this.description; } 
}
