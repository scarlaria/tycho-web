export class CategoryEntity {
  private id : string;
  private name : string;
  private description : string;
  
  public constructor() {
    
  }

    public get Id() { return this.id;}
    public set Id(id : string) {this.id = id;}
    public set Name(name: string){this.name = name;}
    public get Name(){return this.name}
    public set Description (description) { this.description = description; } 
    public get Description () { return this.description; } 

    public static hyperInit(category : CategoryEntity ) {
      if(!category.Id) { category.Id = ''; }
      if(!category.Name) { category.Name = ''; }
      if(!category.Description) { category.Description = ''; }
      return category;
    }

    public static convertObjectToExerciseCategory(key: string, obj : {}) : CategoryEntity {
      let category = CategoryEntity.hyperInit(new CategoryEntity());
      category.Id = key;
      if(obj['name']) { category.Name = obj['name']; }
      if(obj['description']) { category.Description = obj['description']; }
      return category;
    }

}
