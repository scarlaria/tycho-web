import { Observable, Subject } from 'rxjs/Rx';

import {ExerciseEntity} from '../exercise/exercise.entity';
import {SetEntity} from '../../../set/entity/set/set.entity';

export class LoggedExerciseEntity {

    private exerciseId: string;
    private exercise$ : Observable<ExerciseEntity>;
    private set : Array<SetEntity>;
    private loggedWorkoutId: string;
    private owner: string;
    private exerciseDate: any;
    private startTime: any;
    private endTime: any;    
    private isRepMeasurable : boolean;
    private isTimeMeasurable : boolean;
    private isWeightMeasurable: boolean;
    private isHeightMeasureable: boolean;
    private isDistanceMeasurable: boolean;  
  
    constructor(){
    }

    


    /**
     * Takes in a logged-exercise from the @logged-exercise table.
     * @param exerciseEntity,
     * @param key 
     * @param obj
     * @returns {LoggedExerciseEntity} 
     */
    public static convertObject(key: string, loggedExercise : {}, trackedExercise?: string, loggedUser?: string, loggedWorkoutId?: string) : LoggedExerciseEntity {
        let rcee = LoggedExerciseEntity.hyperInit(new LoggedExerciseEntity());    
        // if(key) {rcee.Id = key}
        if(trackedExercise) {rcee.ExerciseId = trackedExercise;}
        if(loggedUser) {rcee.Owner = loggedUser;}
        if(loggedWorkoutId) {rcee.LoggedWorkoutId = loggedWorkoutId;}

        /**Retrieve keys for each set */
        if(loggedExercise[trackedExercise]) {
            let keys = Object.keys(loggedExercise[trackedExercise]);
            keys.forEach(key => {
                let set = loggedExercise[trackedExercise][key];
                let setEntity = SetEntity.convertFromObject(set, key);
                rcee.Set.push(setEntity);
            })
        }
        return rcee;  
    }

    public static hyperInit(entity: LoggedExerciseEntity, numberOfSets?: number) : LoggedExerciseEntity {
        if( !entity.ExerciseId ) { entity.ExerciseId = ''; }
        if( !entity.LoggedWorkoutId ) { entity.LoggedWorkoutId = ''; }
        if( !entity.Owner ) { entity.Owner = ''; }
        if( !entity.Exercise$ ) { entity.Exercise$ = new Observable<ExerciseEntity>(x=>x); }
        if( !entity.StartTime ) { entity.StartTime = Date.now(); }
        if( !entity.EndTime ) {entity.EndTime = Date.now(); }
        if( !entity.ExerciseDate ) { entity.ExerciseDate = Date.now(); }
        if( !entity.Set ) {
            entity.Set = new Array<SetEntity>();
            if(numberOfSets && numberOfSets > 0) {
                for(let i = 0; i < numberOfSets; i++) {
                    let set = SetEntity.hyperInit(new SetEntity())
                    entity.Set.push(set);
                }   
            }
        }
        if( !entity.Exercise$) { entity.Exercise$ = new Subject<ExerciseEntity>();}
        return entity;
    }

	public get ExerciseId(): string {
		return this.exerciseId;
	}

	public set ExerciseId(value: string) {
		this.exerciseId = value;
	}

	public get Set(): Array<SetEntity> {
		return this.set;
	}

	public set Set(value: Array<SetEntity>) {
		this.set = value;
	}

	public get LoggedWorkoutId(): string {
		return this.loggedWorkoutId;
	}

	public set LoggedWorkoutId(value: string) {
		this.loggedWorkoutId = value;
	}

	public get Owner(): string {
		return this.owner;
	}

	public set Owner(value: string) {
		this.owner = value;
	}


    public get Exercise$(): Observable<ExerciseEntity> {
        return this.exercise$;
    }

    public set Exercise$(value : Observable<ExerciseEntity>) {
        this.exercise$ = value;
    }

        public get IsRepMeasurable(): boolean {
  		return this.isRepMeasurable;
  	}

  	public set IsRepMeasurable(value: boolean) {
  		this.isRepMeasurable = value;
  	}


  	public get IsTimeMeasurable(): boolean {
  		return this.isTimeMeasurable;
  	}

  	public set IsTimeMeasurable(value: boolean) {
  		this.isTimeMeasurable = value;
  	}


  	public get IsWeightMeasurable(): boolean {
  		return this.isWeightMeasurable;
  	}

	public set IsWeightMeasurable(value: boolean) {
		this.isWeightMeasurable = value;
	}
    	public get IsHeightMeasurable(): boolean {
		return this.isHeightMeasureable;
	}

	public set IsHeightMeasurable(value: boolean) {
		this.isHeightMeasureable = value;
	}
    

	public get IsDistanceMeasurable(): boolean {
		return this.isDistanceMeasurable;
	}

	public set IsDistanceMeasurable(value: boolean) {
		this.isDistanceMeasurable = value;
	}

    public get EndTime(): any {
		return this.endTime;
	}

	public set EndTime(value: any) {
		this.endTime = value;
	}
    
	public get StartTime(): any {
		return this.startTime;
	}

	public set StartTime(value: any) {
		this.startTime = value;
	}
    

	public get ExerciseDate(): any {
		return this.exerciseDate;
	}

	public set ExerciseDate(value: any) {
		this.exerciseDate = value;
	}

}