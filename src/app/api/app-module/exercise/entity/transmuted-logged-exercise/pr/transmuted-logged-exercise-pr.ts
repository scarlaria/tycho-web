import {LoggedExerciseEntity} from '../../logged-exercise/logged-exercise.entity';
import {TransmutedLoggedExerciseEntity} from '../transmuted-logged-exercise.entity';

export class TransmutedLoggedExercisePR extends TransmutedLoggedExerciseEntity {
    private prValue: {max: number; index: number;}
    private measure: string;

    constructor(loggedExercise: LoggedExerciseEntity){
        super(loggedExercise);
		this.prValue = {max: -1, index: -1};
		this.measure = '';
    }

    public get PRValue(): {max: number; index: number;}  {
		return this.prValue;
	}

	public set PRValue(value: {max: number; index: number;}) {
		this.prValue = value;
	}

	public get Measure(): string {
		return this.measure;
	}

	public set Measure(value: string) {
		this.measure = value;
	}
}