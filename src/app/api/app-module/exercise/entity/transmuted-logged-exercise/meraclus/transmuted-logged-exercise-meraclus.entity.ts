import {LoggedExerciseEntity} from '../../logged-exercise/logged-exercise.entity';

export class TransmutedLoggedExerciseMeraclusEntity 
// extends TransmutedLoggedExerciseEntity
 {
    private measure: string;
    
    constructor(loggedExercise: LoggedExerciseEntity) {
        // super(loggedExercise);
    }

	public get Measure(): string {
		return this.measure;
	}

	public set Measure(value: string) {
		this.measure = value;
	}
}