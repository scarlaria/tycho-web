import {LoggedExerciseEntity} from '../../logged-exercise/logged-exercise.entity';
import {TransmutedLoggedExerciseEntity} from '../transmuted-logged-exercise.entity';

export class TransmutedLoggedExerciseAverage extends TransmutedLoggedExerciseEntity{
    protected averageValue: number;
    protected measure: string;  
    
    constructor(loggedExercise: LoggedExerciseEntity){
        super(loggedExercise);
        // this.loggedExercise = loggedExercise;
    }

    static hyperInit(tleA: TransmutedLoggedExerciseAverage){
        // if(!tleA.AverageValue) {tleA.AverageValue = 0}
        // if(!tleA.LoggedExercise) {tleA.LoggedExercise = new LoggedExerciseEntity()}
        // if(!tleA.Measure) {tleA.Measure = ''}
        // return tleA;
    }

    public get AverageValue(): number {
		return this.averageValue;
	}

	public set AverageValue(value: number) {
		this.averageValue = value;
	}  

    public get Measure(): string {
		return this.measure;
	}

	public set Measure(value: string) {
		this.measure = value;
	}
}