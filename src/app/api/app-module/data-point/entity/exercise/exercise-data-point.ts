

// export class ExerciseDataPoint extends AbstractDataPoint<DependentYEntity, IndependentXEntity>{

//     private measureX: string;
// 	private measureY: string;
// 	private isMetric: boolean;
// 	private loggedExercise: LoggedExerciseEntity;

//     public constructor(dependent: DependentYEntity,independent: IndependentXEntity, measureY: string, measureX: string, loggedExercise: LoggedExerciseEntity, dependentAxisLabel?: string, indepentAxisLabel?: string, isMetric?: boolean) {
// 		super(dependent, independent, dependentAxisLabel, indepentAxisLabel);
//         this.measureX = measureX;
// 		this.measureY = measureY;
// 		this.isMetric = isMetric;
// 		this.loggedExercise = loggedExercise;
//     }

// 	public get LoggedExercise(): LoggedExerciseEntity {
// 		return this.loggedExercise;
// 	}

// 	public set LoggedExercise(value: LoggedExerciseEntity) {
// 		this.loggedExercise = value;
// 	}

// 	public get Dependent(): DependentYEntity {
// 		return this.dependent;
// 	}

// 	public set Dependent(value: DependentYEntity) {
// 		this.dependent = value;
// 	}

// 	public get Independent(): IndependentXEntity {
// 		return this.independent;
// 	}

// 	public set Independent(value: IndependentXEntity) {
// 		this.independent = value;
// 	}

// 	public get Measure(): string {
// 		return this.measureX;
// 	}

// 	public set Measure(value: string) {
// 		this.measureX = value;
// 	}

// 	public get IsMetric(): boolean {
// 		return this.isMetric;
// 	}

// 	public set IsMetric(value: boolean) {
// 		this.isMetric = value;
// 	}

// 	public get MeasureY(): string {
// 		return this.measureY;
// 	}

// 	public set MeasureY(value: string) {
// 		this.measureY = value;
// 	}
    
// }