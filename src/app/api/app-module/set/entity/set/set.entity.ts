/**
 * Created by MartinOO on 20/01/2017.
 */
import { Component } from '@angular/core';

@Component({
})

export class SetEntity {
  private id : string;
  private reps : number;
  private weight: number;
  private time : number;
  private comment : string;
  private like : number;
  private exercise : string;
  private date : any;
  private distance: any;
  private isMetric: boolean;
  private height: number;
 

  public static convertFromObject(obj : {}, id? : string) : SetEntity {
    let set = SetEntity.hyperInit(new SetEntity);
    set.Id = id;
    if(obj['reps']) set.Reps = obj['reps']
    if(obj['weight']) set.Weight = obj['weight']
    if(obj['time']) set.Time = obj['time']
    if(obj['comment']) set.Comment = obj['comment']
    if(obj['like']) set.Like = obj['like']
    if(obj['date']) set.Date = obj['date']
    if(obj['exercise']) set.Exercise = obj['exercise']
    if(obj['distance']) set.Distance = obj['distance']
    if(obj['isMetric']) set.IsMetric = obj['isMetric']
    if(obj['height']) set.IsMetric = obj['height']

    return set;
  }

  public static hyperInit(set : SetEntity) {
    if(!set.Id) { set.Id = ''};
    if(!set.Comment) { set.Id = ''};
    if(!set.Date) { set.Date = ''};
    if(!set.Time) { set.Time = -1};
    if(!set.Like) { set.Like = -1};
    if(!set.Reps) { set.Reps = -1};
    if(!set.Weight) { set.Weight = -1};
    if(!set.Exercise) { set.Exercise = ''};
    if(!set.Distance) { set.Distance = -1};
    if(!set.IsMetric) { set.IsMetric = true};
    if(!set.Height) { set.Height = -1};
    return set;
  }

  public get Distance(): any {
		return this.distance;
	}

	public set Distance(value: any) {
		this.distance = value;
	}

	public get IsMetric(): boolean {
		return this.isMetric;
	}

	public set IsMetric(value: boolean) {
		this.isMetric = value;
	}

    public get Date():any {
        return this.date;
    }

    public set Date(value: any) {
        this.date = value;
    }

    public get Exercise(): string {
        return this.exercise;
    }

    public set Exercise(value:string) {
        this.exercise = value;
    }

    public set Reps (reps) { this.reps = reps; } 
    public get Reps () { return this.reps; } 
    public set Weight (weight) { this.weight = weight; } 
    public get Weight () { return this.weight; } 
    public set Time (time) { this.time = time; } 
    public get Time () { return this.time; } 
    public set Comment (comment) { this.comment = comment; } 
    public get Comment () { return this.comment; } 
    public get Id() { return this.id;}
    public set Id(id : string) {this.id = id;}

	public get Like(): number {
		return this.like;
	}

	public set Like(value: number) {
		this.like = value;
	}
    public get Height(): number {
		return this.height;
	}

    public set Height(value: number) {
        this.height = value;
    }

}
