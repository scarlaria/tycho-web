import {MEASURE_DISTANCE, MEASURE_HEIGHT, MEASURE_REPS, MEASURE_TIME, MEASURE_WEIGHT} from '../../../../../globals/const/global.const';

import { Injectable } from '@angular/core';
import {LoggedExerciseEntity} from '../../../exercise/entity/logged-exercise/logged-exercise.entity';
import { Observable } from 'rxjs/Rx';

export interface IPurgeSet {
    purgeSet(loggedExercise: LoggedExerciseEntity, measure: string): Array<number>
}

@Injectable()
export class SetService implements IPurgeSet {

    private dependentY: string;
    private independentX: string;

    constructor() { }
    
    /**
     * Retrieves the set values from a set based on a measure, and returns those values as an array.
     * If the measure is empty or less than 0, it should be ignored entirely
     * *TESTED
     * 
     * @param {LoggedExerciseEntity} loggedExercise 
     * @param {string} measure 
     * @returns {Array<number>} 
     * 
     * @memberOf SetService
     */
    public purgeSet(loggedExercise: LoggedExerciseEntity, measure: string): Array<number>{
        let arr = new Array<number>();
        if(measure == MEASURE_WEIGHT) {
            loggedExercise.Set.forEach((set,index)=> {
                if(set.Weight && set.Weight > -1){
                    arr.push(set.Weight)
                }         
            });
        }else if(measure == MEASURE_HEIGHT){
                loggedExercise.Set.forEach((set,index)=> {
                    if(set.Height && set.Height > -1){
                        arr.push(set.Height)
                    }      
            });
        }
        else if(measure == MEASURE_DISTANCE){
                loggedExercise.Set.forEach((set,index)=> {
                    if(set.Distance && set.Distance > -1){
                        arr.push(set.Distance)
                    }  
                });
        }
        else if(measure == MEASURE_TIME){
                 loggedExercise.Set.forEach((set,index)=> {
                    if(set.Time && set.Time > -1){
                        arr.push(set.Time)
                    }  
                });           
        }
        else if(measure == MEASURE_REPS){
                loggedExercise.Set.forEach((set,index)=> {
                    if(set.Reps && set.Reps > 0){
                        arr.push(set.Reps)
                    }  
                });            
        }
        return arr; 
    }

    static retrieveMeasureAxes(loggedExercise: LoggedExerciseEntity, dependentY: string, independentX: string): {'dependentY': any[], 'independentX': any[]} {
        let response = {'dependentY': [], 'independentX': []}
        loggedExercise.Set.forEach((set,index) => {
            switch(dependentY.toLocaleLowerCase()) {
                case MEASURE_WEIGHT:
                    response['dependentY'].push(set.Weight)
                case MEASURE_HEIGHT:
                    response['dependentY'].push(set.Height)
                case MEASURE_DISTANCE:
                    response['dependentY'].push(set.Distance)
                case MEASURE_TIME:
                    response['dependentY'].push(set.Time)
                case MEASURE_REPS:
                    response['dependentY'].push(set.Reps)
                default:
                    response['dependentY'].push(-1)
            }

            switch(independentX.toLocaleLowerCase()) {
                case MEASURE_WEIGHT:
                    response['independentX'].push(set.Weight)
                case MEASURE_HEIGHT:
                    response['independentX'].push(set.Height)
                case MEASURE_DISTANCE:
                    response['independentX'].push(set.Distance)
                case MEASURE_TIME:
                    response['independentX'].push(set.Time)
                case MEASURE_REPS:
                    response['independentX'].push(set.Reps)
                default:
                    response['independentX'].push(-1)
            }            
        })
        return response;
    }

    // static retrieveDataPoints(loggedExercise: LoggedExerciseEntity, dependentY: string, independentX: string): ExerciseDataPoint[] {
    //     let dataPoints = Array<ExerciseDataPoint>(loggedExercise.Set.length);
    //     loggedExercise.Set.forEach((set,index) => {
    //         dataPoints.forEach((dataPoint,index) => {              
    //             // switch(dependentY.toLocaleLowerCase()) {
    //             //     case MEASURE_WEIGHT:
    //             //         dataPoint.Dependent = set.Weight;
    //             //         dataPoint.IsMetric = set.IsMetric;
    //             //         dataPoint.DependentAxisLabel = MEASURE_WEIGHT;
    //             //     case MEASURE_HEIGHT:
    //             //         dataPoint.Dependent = set.Height;
    //             //         dataPoint.IsMetric = set.IsMetric;
    //             //         dataPoint.DependentAxisLabel = MEASURE_HEIGHT;                       
    //             //     case MEASURE_DISTANCE:
    //             //         dataPoint.Dependent = set.Distance;
    //             //         dataPoint.IsMetric = set.IsMetric;
    //             //         dataPoint.DependentAxisLabel = MEASURE_DISTANCE;
    //             //     case MEASURE_TIME:
    //             //         dataPoint.Dependent = set.Time;
    //             //         dataPoint.IsMetric = set.IsMetric;
    //             //         dataPoint.DependentAxisLabel = MEASURE_TIME;
    //             //     case MEASURE_REPS:
    //             //         dataPoint.Dependent = set.Reps;
    //             //         dataPoint.IsMetric = set.IsMetric;
    //             //         dataPoint.DependentAxisLabel = MEASURE_REPS;
    //             //     default:
    //             //         dataPoint.Dependent = -1;
    //             //         dataPoint.IsMetric = false;
    //             //         dataPoint.DependentAxisLabel = 'ERROR';
    //             // }

    //             // switch(independentX.toLocaleLowerCase()) {
    //             //     case MEASURE_WEIGHT:
    //             //         dataPoint.Independent = set.Weight;
    //             //         dataPoint.IsMetric = set.IsMetric;
    //             //         dataPoint.IndependentAxisLabel = MEASURE_WEIGHT; 
    //             //     case MEASURE_HEIGHT:
    //             //         dataPoint.Independent =set.Height;
    //             //         dataPoint.IsMetric = set.IsMetric;
    //             //         dataPoint.IndependentAxisLabel = MEASURE_HEIGHT; 
    //             //     case MEASURE_DISTANCE:
    //             //         dataPoint.Independent =set.Distance;
    //             //         dataPoint.IsMetric = set.IsMetric;
    //             //         dataPoint.IndependentAxisLabel = MEASURE_DISTANCE; 
    //             //     case MEASURE_TIME:
    //             //         dataPoint.Independent =set.Time;
    //             //         dataPoint.IsMetric = set.IsMetric;
    //             //         dataPoint.IndependentAxisLabel = MEASURE_TIME; 
    //             //     case MEASURE_REPS:
    //             //         dataPoint.Independent =set.Reps;
    //             //         dataPoint.IsMetric = set.IsMetric;
    //             //         dataPoint.IndependentAxisLabel = MEASURE_REPS; 
    //             //     default:
    //             //         dataPoint.Independent =-1;
    //             // }          
    //         })  
    //     })
    //     return dataPoints;
    // }
    
}