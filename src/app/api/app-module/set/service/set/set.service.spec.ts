import {it} from '@angular-cli/ast-tools';
import { MEASURE_DISTANCE, MEASURE_HEIGHT, MEASURE_WEIGHT } from '../../../../global/service/global.service';
import {LoggedExerciseEntity} from '../../../entity/logged-exercise.entity';
import {SetEntity} from '../../../../workout/entity/set.entity';
import {SetService} from './set.service';

describe('SetService test', ()=>{
  let setService: SetService;
  let loggedExercise: LoggedExerciseEntity;

  beforeEach(()=>{
    setService = new SetService();
    loggedExercise = LoggedExerciseEntity.hyperInit(new LoggedExerciseEntity);
    let set = new SetEntity();set.Weight = 120;set.Height = 10;set.Time = -1;
    let set2 = new SetEntity();set2.Weight = 150;set2.Height = 30;set2.Distance = -1;;set.Reps = 14;set.Time = -1;
    let set3 = new SetEntity();set.Reps = 14;set.Time = -1;set2.Weight = 0;
    let set4 = new SetEntity();set.Time = -1;set.Reps = 19;
    // let set5 = new SetEntity();set5.Weight = -1;set5.Height = -1;set5.Distance = -1;set.Reps = -1;set.Time = -1;
    // let set6 = new SetEntity();set.Reps = 14;set.Time = -1;
    // let set7 = new SetEntity();set7.Distance = -1;
    loggedExercise.Set.push(set,set2,set3,set4);
  })

  it('#purgeSet Height = 10', ()=>{
    let purge = setService.purgeSet(loggedExercise, MEASURE_HEIGHT)
    expect(purge[0]).toBe(10);
  })

  it('#purgeSet Height = 10', ()=>{
    let purge = setService.purgeSet(loggedExercise, MEASURE_HEIGHT)
    expect(purge[1]).toBe(30);
  })

  it('#purgeSet Distance = N/A/[]', ()=>{
    let purge = setService.purgeSet(loggedExercise, MEASURE_DISTANCE)
    expect(purge[0]).toBe([]);
    expect(purge.length).toBeLessThan(1);
  })

  it('#purgeSet Reps to work', ()=>{
    let purge = setService.purgeSet(loggedExercise, MEASURE_WEIGHT)
    expect(purge[0]).toBe([120]);
    expect(purge[2]).toBe([0]);
  })

  it('#purgeSet Weight to work', ()=>{
    let purge = setService.purgeSet(loggedExercise, MEASURE_WEIGHT)
    expect(purge[0]).toBe([14]);
    expect(purge[1]).toBe([14]);
    expect(purge[2]).toBe([19]);
  })
})