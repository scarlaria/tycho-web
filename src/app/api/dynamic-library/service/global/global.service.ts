import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Rx'

@Injectable()
export class GlobalService  {
   
    static getAverage(array: Array<number>): number{
        let sum = 0;
        if(array.length > 1) {
            for(var i = 0, l = array.length; i < l; i++){
                sum += array[i]; //don't forget to add the base
            }
            let average = sum/array.length
            return sum/array.length;
        }else{
            return 0;
        }
    }

    static toArray(obs$: Observable<{}>): Observable<{}[]> {
        let arr = new Array<{}>();
        return obs$.map(x=> {arr.push(x); return arr;})
    }

    /**
     * The lookUp is the element thats being typed in. the containedItem is the value you are searching for
     * 
     * @static
     * @param {string} lookUp 
     * @param {string} containedItem 
     * @returns {boolean} 
     * 
     * @memberOf GlobalService
     */
    public static search(lookUp: string, containedItem: string): boolean {
		if((containedItem.toLowerCase()).search(lookUp.toLowerCase()) != -1) {
			return true;
		}else{
			return false
		};
	}

    public static getMax(arr: Array<number>): {max: number, index: number} {
        let max = -10000;
        let index = -1;
        arr.forEach((num,i) => {
            if(num > max){
                max = num;
                index = i;
            }
        });
        return {max, index};
    }

}
