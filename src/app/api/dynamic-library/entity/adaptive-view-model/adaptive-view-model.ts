import {Observable} from 'rxjs/Rx'
/**
 * An @AdaptiveViewModel is a class that embodies a view-element. The class comprises of variables
 * that allow for the carrying and manipulation of view data. When displaying an AdaptiveViewModel 
 * the contents of the entity are displayed, no other variable within the class can display the contents
 */
export class AdaptiveViewModel<T> {
    /** This acts as a name for the element e.g Category Button Group*/
    private elementName: string;
    /** This better describes the elementName */
    private elementDescription : string;
    /** ClassInfo allows for dynamic manipulation of an html class variable using ngClass[] */
    private classInfo : {}
    /** StyleInfo allows for dynamic manipulation of an html style varuable using ngStyle[] */
    private styleInfo : {}
    /** In order to keep track of a view model if several are being created simultaneously*/
    private index : number;
    /** The entity within an adaptive view model is essential as it contains the information that 
     * all the other variable e.g @classInfo will be placed on. The entity contains the text that will be displayed to the view
     * If a view model manipulates or contains an entity, the entity can replace the generic entity of the class*/
    private entity : T;
    /** View models may contain several entities these can be added to the array*/
    private entitys: T[];

    /**
     * The AdaptiveViewModel constructor takes in an @index that is used to identify 
     * view models that have been clustered together, @classInfo representing a single entities class information
     * and most importantly an @entity that represents the data being displayed on the view. The entity is the crux of an 
     * adaptive view model.
     * @param index 
     * @param classInfo 
     * @param entity 
     */
    constructor(index: number, classInfo : {}, entity : T) {
        this.index = index;
        this.classInfo = classInfo;
        this.entity = entity;
    }

    public fromObservableToObservable(observable$: Observable<{}[]>, classInfo: {}, isFirstSelected?: boolean): Observable<AdaptiveViewModel<{}>> {
        return observable$.flatMap(objects => objects)
                          .map((object,index) => {return new AdaptiveViewModel(index, classInfo, Observable)})
    }

    public fromObservable(observable$: Observable<{}[]>, classInfo: {}, isFirstSelected?: boolean): Array<AdaptiveViewModel<{}>> {
        let arr = new Array<AdaptiveViewModel<{}>>();
        observable$.flatMap(objects => objects)
                          .map((object,index) => {let avm = new AdaptiveViewModel(index, classInfo, object); arr.push(avm); return avm})
                          return arr;
    }

    public morphToAdaptiveViewModelsWithClassInfo(genericClassInfo : {}, specimens : T[]) : AdaptiveViewModel<T>[] {
		let aVMs = new Array<AdaptiveViewModel<T>>();
		specimens.forEach((specimen, index) => {
            aVMs.push(new AdaptiveViewModel(index, genericClassInfo, specimen));
        })
        return aVMs;
    } 

    public morphToAdaptiveViewModels(specimens : T[]) : AdaptiveViewModel<T>[] {
		let aVMs = new Array<AdaptiveViewModel<T>>();
		specimens.forEach((specimen, index) => {
            aVMs.push(new AdaptiveViewModel(index, {}, specimen));
        })
        return aVMs;
	}

    // public convertObject(object: {}, index: number): AdaptiveViewModel<{}> {
    //     return new AdaptiveViewModel(index,object, object)
    // }

    public get Entity() : T {
        return this.entity;
    }

    public set Entity(entity : T) {
        this.entity = entity;
    }

    public get Entitys() : T[] {
        return this.entitys;
    }

    public set Entitys(entitys : T[]) {
        this.entitys = entitys;
    }
    
	public get Index(): number {
		return this.index;
	}

	public set Index(value: number) {
		this.index = value;
	}
    
	public get ClassInfo(): {} {
		return this.classInfo;
	}

	public set ClassInfo(value: {}) {
		this.classInfo = value;
	}

    public get StyleInfo(): {} {
		return this.styleInfo;
	}

	public set StyleInfo(value: {}) {
		this.styleInfo = value;
	}

	public get ElementName(): string {
		return this.elementName;
	}

	public set ElementName(value: string) {
		this.elementName = value;
	}

	public get ElementDescription(): string {
		return this.elementDescription;
	}

	public set ElementDescription(value: string) {
		this.elementDescription = value;
	}
}