/**
 * 
 * Very useful class that eliminates null elements from an object
 * @export
 * @class Objectifier
 */
export class Objectifier {
    private object: Object;

    /**
     * Creates an instance of Objectifier by removing null elements from an object
     * @param {{}} obj 
     * 
     * @memberOf Objectifier
     */
    public constructor(obj: {}) {
        let keys = Object.keys(obj);
        keys.forEach(key => {
            if(obj[key] == null || obj[key] == undefined) {
                delete obj[key];
            }
        })
        this.object = obj;
    }

    /**
     * Rids the input object of null and undefined fields
     * 
     * @static
     * @param {{}} obj 
     * @returns {{}} 
     * 
     * @memberOf Objectifier
     */
    public static objectify = (obj) => {
        Object.keys(obj).forEach(key => {
        if (obj[key] && typeof obj[key] === 'object') Objectifier.objectify(obj[key]);
        else if (obj[key] == null) delete obj[key];
        });
        return obj;
    };



    public get Object(): Object {
		return this.object;
	}

	public set Object(value: Object) {
		this.object = value;
	}
}
