import {CategoryService} from '../../../api/app-module/exercise/service/generic/exercise/category/category.service';
import {EquipmentService} from '../../../api/app-module/exercise/service/generic/exercise/equipment/equipment.service';
import {FirebaseObjectObservable} from 'angularfire2/database';
import { Injectable } from '@angular/core';
import {MeasureService} from '../../../api/app-module/exercise/service/generic/exercise/measure/measure.service';
import {Observable} from 'rxjs/Rx';

@Injectable()
export class InitializerService {
 public static loggedUser: FirebaseObjectObservable<any[]>;
    public ExerciseMeasures: Observable<{}[]>;
    public ExerciseEquipments: Observable<{}[]>;
    public ExerciseCategorys: Observable<{}[]>;

    constructor(private mS: MeasureService, 
                private eqS: EquipmentService, private cS: CategoryService) {
        this.ExerciseMeasures = mS.retrieveAllMeasures();
        this.ExerciseEquipments = eqS.retrieveAllEquipments()
        this.ExerciseCategorys = cS.retrieveAllCategorys()
    }
}