/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { GlobalSettingsService } from './global-settings.service';

describe('Service: GlobalSettings', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GlobalSettingsService]
    });
  });

  it('should ...', inject([GlobalSettingsService], (service: GlobalSettingsService) => {
    expect(service).toBeTruthy();
  }));
});