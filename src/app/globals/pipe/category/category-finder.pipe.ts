import { Pipe, PipeTransform } from '@angular/core';

import { InitializerService } from '../../service/initializer/initializer.service';

@Pipe({
  name: 'category-description'
})
export class CategoryDescriptionPipe implements PipeTransform {
  public constructor(private iS: InitializerService) {
    iS.ExerciseCategorys.subscribe(x=>console.log(x));
  }
  transform(key: string): any {
    return '';
  }
}

@Pipe({
  name: 'category-name'
})
export class CategoryNamePipe implements PipeTransform {
  public constructor(private iS: InitializerService) {
    iS.ExerciseCategorys.subscribe(x=>console.log(x));
  }
  transform(key: string): any {
    return '';
  }
}