export const MEASURE_WEIGHT = "weight";
export const MEASURE_HEIGHT = "height";
export const MEASURE_DISTANCE = "distance";
export const MEASURE_REPS = "reps";
export const MEASURE_TIME = "time";

export const DB_USER = '/user';
export const DB_WORKOUT = '/workout';
export const DB_EXERCISE = '/exercise';
export const DB_GOAL_PR = '/goalpr';
export const DB_EXERCISE_EQUIPMENT = '/exercise-equipment';
export const DB_LOGGED_WORKOUT = '/logged-workout';
export const DB_EXERCISE_CATEGORY = '/exercise-category';
export const DB_EXERCISE_MEASURE = '/exercise-measure';
export const DB_LOGGED_EXERCISE = '/logged-exercise';
export const DB_DASH = '/';
export const DB_LOGGED_EXERCISE_BY_USER = '/logged-exercise-by-user';
export const DB_LOGGED_WORKOUT_BY_USER = '/logged-workout-by-user';
export const DB_LOGGED_EXERCISE_BY_LOGGED_WORKOUT = '/logged-exercise-by-logged-workout';
export const DB_ANATOMY = '/anatomy';
export const DB_EXERCISE_GLOBAL_COMPLETION = '/globalCompletion';

/**GOALPR */
export const ROUTE_CREATE_GOAL ='create-goal';
export const ROUTE_GOALPR = 'goals-pr';
export const ROUTE_DASH = '/';
/**EXERCISE */
export const ROUTE_EXERCISE = 'exercises';
export const ROUTE_CREATE_EXERCISE = 'create-exercise';
export const ROUTE_UPDATE_EXERCISE = 'update-exercise';
export const ROUTE_VIEW_EXERCISE = 'view-exercise';