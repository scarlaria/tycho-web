import { AUTH_PROVIDERS, AngularFireAuthModule } from 'angularfire2/auth';
import {RouterModule, Routes} from '@angular/router';

import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireModule } from 'angularfire2';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';
import {AsideToggleDirective} from './core/layout/shared/aside.directive';
import {BreadcrumbsComponent} from './core/layout/shared/breadcrumb.component';
import { BrowserModule } from '@angular/platform-browser';
import {DashboardSidebarComponent} from './core/layout/layouts/sidebar.component';
import { ExerciseModule } from './core/app-module/exercise/exercise.module';
import { FormsModule } from '@angular/forms';
import {FullLayoutComponent} from './core/layout/layouts/full-layout.component';
import { GlobalService } from './api/dynamic-library/service/global/global.service';
import { GlobalSettingsService } from './globals/service/settings/global-settings.service';
import { HttpModule } from '@angular/http';
import { InitializerService } from './globals/service/initializer/initializer.service';
import {NAV_DROPDOWN_DIRECTIVES} from './core/layout/shared/nav-dropdown.directive';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {SIDEBAR_TOGGLE_DIRECTIVES} from './core/layout/shared/sidebar.directive';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    FullLayoutComponent,
    DashboardSidebarComponent,
    NAV_DROPDOWN_DIRECTIVES,
    BreadcrumbsComponent,
    SIDEBAR_TOGGLE_DIRECTIVES,
    AsideToggleDirective,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    NgbModule.forRoot(),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFireDatabaseModule,

    ExerciseModule,

    AppRoutingModule
  ],
  providers: [GlobalSettingsService, InitializerService, GlobalService],
  bootstrap: [AppComponent]
})
export class AppModule { }
