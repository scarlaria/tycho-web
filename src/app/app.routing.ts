import { RouterModule, Routes } from '@angular/router';

//Layouts
import { FullLayoutComponent } from './core/layout/layouts/full-layout.component';
import {NgModule} from '@angular/core';

// import {ExerciseComponent} from './exercise/exercise.component';
// import {LoginComponent} from './authentication/submodule/login/login.component';
// import {TestlabComponent} from './testlab/testlab.component';

export const routes: Routes = [
  // {
  //   path: 'login',
  //   component: LoginComponent,
  // },
  // {
  //   path: 'testlab',
  //   component: TestlabComponent,
  // },
  // {
  //   path: 'exercises',
  //   component: ExerciseComponent
  // },
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: '',
    component: FullLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      // {
      //   path: 'dashboard',
      //   loadChildren: './dashboard/dashboard.module#DashboardModule'
      // },
      // {
      //   path: 'training',
      //   loadChildren: './training/training.module#TrainingModule'
      // },
      {
        path: 'exercises',
        loadChildren: './core/app-module/exercise/exercise.module#ExerciseModule'
      },
      // {
      //   path: 'workouts',
      //   loadChildren: './workout/workout.module#WorkoutModule'
      // },
      // {
      //   path: 'personal-record',
      //   component: PersonalRecordComponent
      // },      
      // {
      //   path: 'profile',
      //   loadChildren: './profile/profile.module#ProfileModule'
      // },
      // {
      //   path: 'goals-pr',
      //   loadChildren: './goalpr/goalpr.module#GoalPRModule'
      // },
    ]
  },

];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
