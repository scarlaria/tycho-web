// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBJJNpdRH7XGGv1jjjwzrAyJ3CQ3MpI91s",
    authDomain: "tycho-fc797.firebaseapp.com",
    databaseURL: "https://tycho-fc797.firebaseio.com",
    storageBucket: "tycho-fc797.appspot.com",
    messagingSenderId: "352384520497"
  }
};
