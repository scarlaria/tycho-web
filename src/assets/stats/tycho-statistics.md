*Exercises*
1. Highest Rated Exercises
2. Most Challenging Exercises
3. Highest Completed Exercise - All Time
4. Highest Completed Exercise - Yearly
5. Highest Completed Exercise - This Month
6. Highest Completed Exercise - This Week
7. Highest Completed Exercise - Today
8. Number of times a user has completed an exercise - FT
9. Highest PR among friends
10. Least favorite exercise
11. Least Completed Exercise - All Time
12. Least Completed Exercise - Yearly
13. Least Completed Exercise - This Month
14. Least Completed Exercise - This Week
15. Least Completed Exercise - Today

