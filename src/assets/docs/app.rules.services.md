# Service Rules

_Since majority of the flow of the applicatin depends on incoming data from a database. All applications should attempt as much as possible to use Observables and other RxJs submodules that enable smoothasynchronous flow_

## 1. Naming Conventions
### 1.1 Services
    There are 3 types of services
    1. General Services
    2. Local Services
    3. Statistical (Statistics) Services
#### 1.1.1 General Services
__format__ : ```exercise.service.ts```
General or Generic services have the role of performing manipulations to the component and entity. They also serve the current purpose of handling database transactions which might later be abstracted to a repository submodule altogether. 
General services are primarly located at module level and contain all the services the entire module requires

#### 1.1.2 Local Services
__format__ : ```exercise.local.ts```
Local services are very different from general services but are crucial. They maintain data held by several subcomponents across the main componenent. For instance, the ```exercise.component.html``` may contain 5+ subcomponents to make up the entire view. All these subcomponents are interacting and manipulating the view as well as shared data. The local service houses data that all these subcomponents can interact ensuring a consistent method. 

Local components **do not** have method functionality for manipulating data eg. ```add(a+b)``` or ```retrieveWorkout(id: string)``` these methods are placed in the general service. Local components for instance house the model ```private exercise : ExerciseEntity``` that will be manipulated via the view.

**Local Services must be declared in the ```providers``` section of the module you are in**

##### Best Practices
To the best of the developers ability, keep the local service as thin and light as possible. Most local services only contain a single ```model``` , a ```currentPage``` identifier as well as the ```totalPages``` to allow ease of navigation. Other component specific information such as ```headerName : string``` should be placed within its respective component.
Local service complexity tends to grow quickly as more components are created. Spend time and plan a local service before implementing one. 

#### 1.1.3 Statistical Services
__format__ : ```exercise.statistics.ts```
The functions contained within statistical services would theoretically be placed within the generic service, however the statistical component of the app is a large submodule that needs proper care. There is no harm in abstracting statistical centric operations. This also helps in providing a pricing strategy.

