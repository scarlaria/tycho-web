import { TychoWebPage } from './app.po';

describe('tycho-web App', () => {
  let page: TychoWebPage;

  beforeEach(() => {
    page = new TychoWebPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
